import { Client } from 'bugsnag-react-native';
const bugsnag = new Client("b9deb8ea37c4fb15eac4228c67930dbb");

import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/redux/store';

import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
import Colors from './src/utils/Colors';

//Begin Global
import Init from './Init';
import Swipes from './src/begin/Swipes';
import Auth from './src/begin/Auth';
import Login from './src/begin/Login';
import VerifyOTP from './src/begin/VerifyOTP';

//Begin Driver
import DriverCategory from './src/begin/driver/DriverCategory';
import DriverPersonalInfo from './src/begin/driver/DriverPersonalInfo';
import DriverBankInfo from './src/begin/driver/DriverBankInfo';
import DriverVehicleInfo from './src/begin/driver/DriverVehicleInfo';
import DriverUpload from './src/begin/driver/DriverUpload';


//Begin User
import UserPersonalInfo from './src/begin/user/UserPersonalInfo';

//Screens Global
import About from './src/screens/About';
import Support from './src/screens/Support';
import Offline from './src/screens/Offline';
import Notifications from './src/screens/Notifications';
import TripDetail from './src/screens/TripDetail';

//Screens Driver
import DriverSideMenu from './src/components/DriverSideMenu';
import DriverHome from './src/screens/driver/DriverHome';
import DriverDeliveries from './src/screens/driver/DriverDeliveries';
import DriverTrips from './src/screens/driver/DriverTrips';
import DriverWallet from './src/screens/driver/DriverWallet';
import DriverProfile from './src/screens/driver/DriverProfile';
import DriverTrip from './src/screens/driver/DriverTrip';
import DriverAcceptDelivery from './src/screens/driver/DriverAcceptDelivery';


//Screens User
import UserSideMenu from './src/components/UserSideMenu';
import UserDeliveries from './src/screens/user/UserDeliveries';
import UserHome from './src/screens/user/UserHome';
import UserPayments from './src/screens/user/UserPayments';
import UserProfile from './src/screens/user/UserProfile';
import UserDeliveryDetails from './src/screens/user/UserDeliveryDetails';
import UserTrip from './src/screens/user/UserTrip';
import UserRateDriver from './src/screens/user/UserRateDriver';
import UserSearchPlace from './src/screens/user/UserSearchPlace';

const DriverMenu = createDrawerNavigator(

    {
        DriverHome: { screen: DriverHome },
        DriverDeliveries: { screen: DriverDeliveries },
        DriverTrips: { screen: DriverTrips },
        DriverTrip: { screen: DriverTrip },
        DriverTripDetail: { screen: TripDetail },
        DriverWallet: { screen: DriverWallet },
        DriverProfile: { screen: DriverProfile },
        DriverAbout: { screen: About },
        DriverSupport: { screen: Support },
        DriverNotifications: { screen: Notifications },
        DriverAcceptDelivery: { screen: DriverAcceptDelivery },

    },
    {
        contentComponent: props => <DriverSideMenu {...props} />
    }
);

const UserMenu = createDrawerNavigator(

    {
        UserHome: { screen: UserHome },
        UserDeliveries: { screen: UserDeliveries },
        UserPayments: { screen: UserPayments },
        UserProfile: { screen: UserProfile },
        UserAbout: { screen: About },
        UserSupport: { screen: Support },
        UserDeliveryDetails: { screen: UserDeliveryDetails },
        UserTrip: { screen: UserTrip },
        UserTripDetail: { screen: TripDetail },
        UserRateDriver: { screen: UserRateDriver },
        UserNotifications: { screen: Notifications },
        UserSearchPlace: { screen: UserSearchPlace },

    },
    {
        contentComponent: props => <UserSideMenu {...props} />
    }
);


const Menu = createStackNavigator(

    {
        Init: Init,

        DriverMenu: DriverMenu,
        UserMenu: UserMenu,

        Swipes: { screen: Swipes },
        Auth: { screen: Auth },
        Login: { screen: Login },
        VerifyOTP: { screen: VerifyOTP },

        DriverPersonalInfo: { screen: DriverPersonalInfo },
        DriverBankInfo: { screen: DriverBankInfo },
        DriverVehicleInfo: { screen: DriverVehicleInfo },
        DriverCategory: { screen: DriverCategory },
        DriverUpload: { screen: DriverUpload },

        UserPersonalInfo: { screen: UserPersonalInfo },
        Offline: { screen: Offline }


    },
    {
        headerMode: 'none',
        navigationOptions: { drawerLockMode: 'locked-closed' }
    }
);

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: Colors.white,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const RootApp = createAppContainer(Menu);

export default class App extends Component {

    loading = () => {

        return (

            <View style={styles.container}>
                <ActivityIndicator size='large' color={Colors.primary} />
            </View>
        )
    };

    render() {

        return (

            <Provider store={store}>
                <PersistGate persistor={persistor} loading={this.loading()}>
                    <RootApp />
                </PersistGate>
            </Provider>
        )
    }

}