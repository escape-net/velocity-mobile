import React, { Component } from 'react';
import { Vibration, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { StackActions, NavigationActions } from 'react-navigation';
import API from './src/utils/API';
import Local from './src/utils/Local';
import { DRIVER_ACCEPT_DELIVERY, USER_TRIP } from './src/redux/types';

class Init extends Component {

    componentDidMount() {

        SplashScreen.hide();

        this.init();
        this.prepareNotifications();
    }

    init = async () => {

        let route = 'Auth';

        if (this.props.api_token == null) {

            route = 'Swipes';

        } else {

            if (this.props.api_token != 'not_set') {

                if (this.props.role == 'user') route = 'UserMenu';
                if (this.props.role == 'driver') route = 'DriverMenu';

                this.firebasePermission();
            }
        }

        this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));
    }

    firebasePermission = async () => {

        try {

            const enabled = await firebase.messaging().hasPermission();

            if (!enabled)
                await firebase.messaging().requestPermission();

        } catch (ex) {

            Local.toast(ex);
        }
    }

    prepareNotifications = async () => {

        try {

            const token = await firebase.messaging().getToken();

            if (token) {

                if (this.props.api_token && this.props.api_token != 'not_set')
                    await API.post('update-device-token', { token: token }, this.props.api_token);

                this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(async token => {

                    if (this.props.api_token && this.props.api_token != 'not_set')
                        await API.post('update-device-token', { token: token }, this.props.api_token);
                });

                this.messageListener = firebase.messaging().onMessage(message => {

                    //console.log('messageListener', message);
                });

                this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed(notification => {

                    //console.log('onNotificationDisplayed', notification);

                });

                this.notificationListener = firebase.notifications().onNotification((notification) => {

                    this.handleNotification(notification);
                });

                this.notificationOpenedListener = firebase.notifications().onNotificationOpened(notification => {

                    this.handleNotification(notification.notification);
                });
            }


        } catch (ex) {

            Local.toast(ex);
        }
    }

    handleNotification = async notification => {

        try {

            let { title, body, data } = notification;
            let payload = JSON.parse(data.payload);

            if (!payload) return;

            let response = await API.get(`delivery/${payload.delivery_id}`, this.props.api_token);

            if (response.status == false)
                return;

            let delivery = response.data;
            //console.log({ delivery: delivery, type: data.type, role: this.props.role, title: title, body: body });

            if (this.props.role == 'driver' && delivery) {

                switch (data.type) {

                    case 'assigned':
                        Vibration.vibrate([1000, 2000, 3000], true);
                        this.props.dispatch({ type: DRIVER_ACCEPT_DELIVERY, payload: { delivery } });
                        this.props.navigation.navigate('DriverAcceptDelivery');
                        break;
                }
            }

            if (this.props.role == 'user' && delivery) {

                Alert.alert(title, body);
                Vibration.vibrate(1000, true);
                setTimeout(() => Vibration.cancel(), 1000);

                switch (data.type) {

                    case 'declined':
                        this.props.dispatch({ type: USER_TRIP, payload: { delivery, trip_title: `Delivery of ${delivery.item}`, searching: true, driver: null, driver_location: null, search: true } });
                        setTimeout(() => this.props.navigation.navigate('UserTrip'), 2000);
                        break;

                    case 'started':
                        let driver = delivery.driver ? delivery.driver : null;
                        this.props.dispatch({ type: USER_TRIP, payload: { delivery, started: true, trip_title: `Delivery of ${delivery.item}`, searching: false, driver: driver, driver_location: null, search: false } });
                        setTimeout(() => this.props.navigation.navigate('UserTrip'), 2000);
                        break;

                    case 'completed':
                        this.props.dispatch({ type: USER_TRIP, payload: null });
                        setTimeout(() => this.props.navigation.navigate('UserRateDriver'), 2000);
                        break;
                }
            }

        } catch (ex) {

            Local.toast(ex);
        }
    }

    componentWillUnmount() {

        try {

            if (this.onTokenRefreshListener)
                this.onTokenRefreshListener();

            if (this.messageListener)
                this.messageListener();

            if (this.notificationDisplayedListener)
                this.notificationDisplayedListener();

            if (this.notificationListener)
                this.notificationListener();

            if (this.notificationOpenedListener)
                this.notificationOpenedListener();

        } catch (ex) {

            Local.toast(ex);
        }

    }


    render() { return null }

}

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };
export default connect(mapStateToProps, mapDispatchToProps)(Init);
