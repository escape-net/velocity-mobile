import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, StyleSheet, ImageBackground, Text, View, TouchableOpacity } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import VButton from '../components/VButton';
import Colors from '../utils/Colors';

class Auth extends Component {

    constructor(props) {

        super(props);
        this.state = { login_loading: false, loading: false, phone: '' };
    }

    componentDidMount() {

        SplashScreen.hide();
    }

    async login() {

        this.props.navigation.navigate('Login');
    }

    async register() {

        this.props.navigation.navigate('DriverCategory');
    }

    render() {

        return (

            <View style={styles.container}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle="dark-content" animated />

                <View style={{ alignItems: 'flex-start', flex: 2, marginTop: 50, marginBottom: 30 }}>
                    <ImageBackground source={require('../img/auth-header.png')} style={{ width: '100%', height: '100%' }}></ImageBackground>
                    <Text style={styles.driver_app}>Velocity Mobile</Text>
                </View>

                <View style={{ marginTop: 0, paddingTop: 20, paddingLeft: 30, paddingRight: 30 }}>

                    <VButton onPress={this.login.bind(this)} text='LOGIN' loading={this.state.login_loading} background={Colors.primary} color={Colors.white}></VButton>

                    <VButton onPress={() => this.props.navigation.navigate('UserPersonalInfo')} text='SIGNUP' loading={this.state.loading} background={Colors.white} color={Colors.primary}></VButton>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DriverCategory')}>
                        <Text style={{ paddingTop: 15, textAlign: 'center', fontSize: 15 }}>Or Register as a <Text style={{ color: Colors.primary }}>Driver</Text></Text>
                    </TouchableOpacity>

                </View>

                <View style={{ padding: 20, justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ textAlign: "center", marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: '#52009f' }}>terms and condition</Text> and <Text style={{ color: '#52009f' }}>privacy policy</Text>
                    </Text>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({

    container: {

        backgroundColor: Colors.white,
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    driver_app: {

        color: Colors.primary,
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 30,
        paddingTop: 10,
        marginBottom: 30
    },
});

export default connect(null, {})(Auth);

