import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, StyleSheet, Alert, Image, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input } from 'native-base';
import VButton from '../components/VButton';
import Colors from '../utils/Colors';
import API from '../utils/API';
import { AUTH } from '../redux/types';

class Login extends Component {

    constructor(props) {

        super(props);
        this.state = { loading: false, phone: '' };
		
    }

    async login() {
        
        try {
          
		
		
            if (this.state.loading == true) return;

            this.setState({ loading: true });

            let response = await API.post('login', this.state);

            if (response.status == true) {

                this.setState({ loading: false });
                this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                this.props.navigation.navigate('VerifyOTP');

            } else {

                console.log(response);
                this.setState({ loading: false });
                Alert.alert('Opzz', response.data.toString());
            }

        } catch (ex) {

            console.log(ex);
            this.setState({ loading: false });
            Alert.alert('Opzz', 'Could not connect at the moment.');
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', padding: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 50 }} source={require('../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Hello!</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Enter your phone number below {'\n'}to get started </Text>
                </View>

                <View style={{ marginTop: 0, padding: 30 }}>
                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Phone Number</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.setState({ phone: text })} value={this.state.phone} />
                    </Item>
                    <VButton onPress={() => this.login()} text='PROCEED' loading={this.state.loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

                <View style={{ paddingTop: 50, paddingLeft: 30, paddingRight: 30, justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: Colors.white,
        height: '100%'
    }
});

const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(null, mapDispatchToProps)(Login);
