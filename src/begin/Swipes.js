import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, AsyncStorage, Image } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import Swiper from './Swiper';
import Colors from '../utils/Colors';
import { AUTH } from '../redux/types';

class Swipes extends Component {

  componentDidMount() {

    this.props.dispatch({ type: AUTH, payload: { api_token: 'not_set' } });
    SplashScreen.hide();

  }

  render() {

    return (

      <Swiper navigation={this.props.navigation}>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode="contain" style={{ width: 300, height: 300 }} source={require('../img/request.png')} />
          <Text style={styles.header}>24 HOURS DELIVERY</Text>
          <Text style={styles.text}>Get your Parcels Delivered within 24hours!</Text>

        </View>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode="contain" style={{ width: 300, height: 300 }} source={require('../img/get-paid.png')} />
          <Text style={styles.header}>BUY YOURSELF MORE TIME</Text>
          <Text style={styles.text}>Let us handle all your deliveries. Spend time doing what matters to you.</Text>

        </View>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode="contain" style={{ width: 300, height: 300 }} source={require('../img/movement.png')} />
          <Text style={styles.header}>JOIN THE MOVEMENT</Text>
          <Text style={styles.text}>Be a part of our journey to make the world a better place</Text>

        </View>

      </Swiper>

    );
  }
}

const styles = StyleSheet.create({

  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    color: Colors.primary,
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 15,
    marginTop: 50,
  },
  text: {
    color: 'grey',
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: 'bold',
    marginHorizontal: 40,
    textAlign: 'center',
  },

});

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Swipes);



