import React, { Component } from 'react';
import { StatusBar, Alert, View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import firebase, { Notification } from 'react-native-firebase';
import Pincode from 'react-native-code-verification';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { DotIndicator } from 'react-native-indicators';
import Colors from '../utils/Colors';
import Local from '../utils/Local';
import API from '../utils/API';
import { AUTH } from '../redux/types';

class VerifyOTP extends Component {

    constructor(props) {

        super(props);
        this.state = { loading: false, count: 1, otp: '' };

    }

    async componentDidMount() {

        try {

            if (!this.props.user) {

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Auth' })] }));

            }

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    async verify(pin = '0000') {

        try {

            if (pin == this.props.user.otp) {

                this.setState({ loading: false });

                this.props.dispatch({

                    type: AUTH,

                    payload: {

                        api_token: this.props.user.api_token,
                        role: this.props.user.role,
                        user: this.props.user
                    }
                });

                const token = await firebase.messaging().getToken();

                console.log(token);

                if (token)
                    await API.post('update-device-token', { token: token }, this.props.api_token);

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Init' })] }));

            } else {

                this.setState({ loading: false });
                Alert.alert('Account Verification Failed', 'Invalid OTP provided');
            }

        } catch (ex) {

            Local.toast(ex.message);

        }

    }

    resendOTP = async () => {

        try {

            if (this.state.count >= 3) {

                Local.toast(`You have exceeded OTP retries.`);
                return;
            }

            let count = this.state.count + 1;
            this.setState({ loading: true, count: count });

            let response = await API.post('login', { phone: this.props.user.phone });

            if (response.status == true) {

                this.setState({ loading: false, user: response.data });
                Local.toast(`OTP has being resent to ${this.props.user.phone}`);

            } else {

                this.setState({ loading: false });
                Alert.alert('Error', response.data.toString());
            }

        } catch (e) {


        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={{ backgroundColor: Colors.white, height: '100%' }} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />
                <Pincode onEnteredPincode={pin => this.verify(pin)} descriptionText='Please enter the OTP sent to your phone' spaceColor={Colors.primary} onCloseView={() => this.props.navigation.navigate('Auth')} withTouchId={false} />

                <View style={styles.footer}>

                    {this.state.loading
                        ? <DotIndicator size={7} color={Colors.red} />
                        : <TouchableOpacity style={{ flex: 1 }} onPress={() => this.resendOTP()}>
                            <Text style={{ textAlign: 'center', color: Colors.red, fontSize: 16 }}>Resend OTP</Text>
                        </TouchableOpacity>
                    }

                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Auth' })] }))}>
                        <Text style={{ textAlign: 'center', color: Colors.primary, fontSize: 16 }}>Change Phone Number</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({

    footer: {
        position: 'absolute',
        top: '90%',
        padding: 30,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        justifyContent: 'space-around'
    }
});


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(VerifyOTP);

