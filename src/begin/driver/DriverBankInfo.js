import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, StyleSheet, Alert, Image, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input, Picker } from 'native-base';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import API from '../../utils/API';
import Local from '../../utils/Local';
import { DRIVER_BANK_INFO } from '../../redux/types';

class DriverBankInfo extends Component {

    constructor(props) {

        super(props);

        this.state = {
            loading: false,
            bank_name: '',
            bank_account_number: '',
            bank_account_name: '',
            email: '',
            banks: null,
            bank_id: null
        };
    }

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_BANK_INFO, payload: state });
    }

    async componentDidMount() {

        try {

            let user = await Local.get('user');

            if (user && typeof user === 'object') {

                this.setState({ bank_account_number: user.bank_account_number, bank_account_name: user.bank_account_name });
            }

            let banks = await Local.get('banks');
            if (banks) this.setState({ banks: banks });

            let response = await API.get('banks');

            if (response.status == true) {

                await Local.save('banks', response.data);
                this.setState({ banks: response.data });
            }

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    async continue() {

        try {

            if (this.state.loading == true) return;

            this.setState({ loading: true });

            let user = await Local.get('user');

            if (user && typeof user === 'object' && user.api_token) {

                let response = await API.post('driver/bank-details', this.state, user.api_token);

                this.setState({ loading: false });
                let { data } = response;
                await Local.save('user', data);
                Alert.alert('Bank Information', 'Your Bank Information has been saved.');
                this.props.navigation.navigate('DriverProfile');

            } else {

                Alert.alert('Error', 'Could not connect at the moment.');
            }


        } catch (ex) {

            this.setState({ loading: false });
            Alert.alert('Error', 'Could not connect at the moment.');
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Velocity Driver</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Please enter your Bank Account Information</Text>
                </View>

                <View style={{ margin: 0, padding: 10 }}>

                    {this.state.banks
                        ? <Picker mode='dropdown' placeholder='Vehicle Type' placeholderStyle={{ color: Colors.black }} placeholderIconColor={Colors.black} style={{ width: undefined }} selectedValue={this.state.bank_id} onValueChange={value => { this.setState({ bank_id: value }) }}>

                            <Picker.Item label='Select Bank' value='0' />

                            {this.state.banks.map(row => (

                                <Picker.Item key={row.id} label={row.name} value={row.id} />
                            ))}
                        </Picker>
                        : null}

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>Bank Account Number</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.setState({ bank_account_number: text })} value={this.state.bank_account_number} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>Bank Account Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ bank_account_name: text })} value={this.state.bank_account_name} />
                    </Item>

                    <VButton onPress={this.continue.bind(this)} text='SAVE' loading={this.state.loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: Colors.white
    }
});


const mapStateToProps = state => ({ ...state.driverBankInfo, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverBankInfo);

