import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, Image, Text, View, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { DRIVER_CATEGORY } from '../../redux/types';

class DriverCategory extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_CATEGORY, payload: state });
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Join Velocity</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Please select a category</Text>
                </View>

                <View style={{ margin: 0, padding: 10 }}>

                    <TouchableOpacity onPress={() => this.updateState({ category: 1 })}>
                        <View style={[styles.card, { backgroundColor: this.props.category == 1 ? Colors.primary : Colors.white }]}>
                            <Text style={[styles.h1, { color: this.props.category == 1 ? Colors.white : Colors.primary }]}>Driving Only</Text>
                            <Text style={{ color: this.props.category == 1 ? Colors.white : Colors.primary }}>You do not have a vehicle but you want to be employed as a Driver</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.updateState({ category: 2 })}>
                        <View style={[styles.card, { backgroundColor: this.props.category == 2 ? Colors.primary : Colors.white }]}>
                            <Text style={[styles.h1, { color: this.props.category == 2 ? Colors.white : Colors.primary }]}>Vehicle Owner and Driving</Text>
                            <Text style={{ color: this.props.category == 2 ? Colors.white : Colors.primary }}>You own a vehicle and will drive yourself.</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.updateState({ category: 3 })}>
                        <View style={[styles.card, { backgroundColor: this.props.category == 3 ? Colors.primary : Colors.white }]}>
                            <Text style={[styles.h1, { color: this.props.category == 3 ? Colors.white : Colors.primary }]}>Vehicle Owner but not Driving</Text>
                            <Text style={{ color: this.props.category == 3 ? Colors.white : Colors.primary }}>You own a vehicle but will employ others to drive your vehicle</Text>
                        </View>
                    </TouchableOpacity>

                    <VButton onPress={() => this.props.navigation.navigate('DriverPersonalInfo')} text='CONTINUE' loading={this.props.driver_category_loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

                <View style={styles.continue}>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.BeginDriverCategory;

const mapStateToProps = state => ({ ...state.driverCategory });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverCategory);