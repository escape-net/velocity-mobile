import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, Alert, Image, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input, Picker } from 'native-base';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { DRIVER_PERSONAL_INFO, AUTH } from '../../redux/types';

class DriverPersonalInfo extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_PERSONAL_INFO, payload: state });
    }

    async componentDidMount() {

        try {

            if (this.props.user.id)
                this.props.navigation.navigate('DriverUpload');

            let response = await API.get('vehicle-types');

            if (response.status == true)
                this.updateState({ vehicle_types: response.data });

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    async register() {

        try {

            if (this.props.driver_personal_info_loading == true) return;

            this.updateState({ driver_personal_info_loading: true });

            let response = await API.post('register', {

                phone: this.props.phone,
                first_name: this.props.first_name,
                last_name: this.props.last_name,
                email: this.props.email,
                role: 'driver',
                category: this.props.category,
                vehicle_type_id: this.props.vehicle_type_id
            });

            if (response.status == true) {

                this.updateState({ driver_personal_info_loading: false });

                let { data } = response;
                this.props.dispatch({ type: AUTH, payload: { user: data, api_token: data.api_token, role: data.role } });

                this.props.navigation.navigate('DriverUpload');

            } else {

                this.updateState({ driver_personal_info_loading: false });
                Alert.alert('Error', response.data.toString());
            }

        } catch (ex) {

            this.updateState({ driver_personal_info_loading: false });
            Alert.alert('Error', ex.message);
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Join Velocity</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Before we begin, let us know a few things about you...</Text>
                </View>

                <View style={{ margin: 0, padding: 10 }}>

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>Phone Number</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ phone: text })} value={this.props.phone} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>First Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ first_name: text })} value={this.props.first_name} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>Last Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ last_name: text })} value={this.props.last_name} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 14 }}>
                        <Label style={{ marginTop: 5 }}>Email Address</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ email: text })} value={this.props.email} />
                    </Item>

                    {this.props.vehicle_types
                        ? <Picker mode='dropdown' placeholder='Vehicle Type' placeholderStyle={{ color: Colors.black }} placeholderIconColor={Colors.black} style={{ width: undefined }} selectedValue={this.props.vehicle_type_id} onValueChange={value => { this.updateState({ vehicle_type_id: value }) }}>

                            <Picker.Item label='Select Vehicle Type' value='0' />

                            {this.props.vehicle_types.map(row => (

                                <Picker.Item key={row.id} label={row.name} value={row.id} />
                            ))}
                        </Picker>
                        : null}

                    <VButton onPress={this.register.bind(this)} text='NEXT' loading={this.props.driver_personal_info_loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

                <View style={styles.continue}>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.BeginDriverPersonalInfo;

const mapStateToProps = state => ({ ...state.driverPersonalInfo, ...state.driverPersonalInfo, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverPersonalInfo);
