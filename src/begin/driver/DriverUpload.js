import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, Image, Text, View, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PhotoUpload from 'react-native-photo-upload';
import { DotIndicator } from 'react-native-indicators';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { DRIVER_UPLOAD, AUTH } from '../../redux/types';

class DriverUpload extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_UPLOAD, payload: state });
    }

    async continue() {

        if (this.props.driver_license == null) {

            return Alert.alert('Drivers License', 'Please Upload your Drivers License to Continue');
        }

        if (this.props.photo == null) {

            return Alert.alert('Profile Photo', 'Please Upload your Profile Photo to Continue');
        }

        if (this.props.vehicle_image == null) {

            return Alert.alert('Profile Photo', 'Please Upload your Vehicle Photo to Continue');
        }

        this.props.navigation.navigate('DriverVehicleInfo');
    }

    async uploadDriversLicense(image) {

        try {

            let response = await API.post('driver/upload/driver_license', { image: image }, this.props.api_token);

            if (response) {

                if (response.status == true) {

                    this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                    this.updateState({ driver_license: response.data.driver_license });

                } else {

                    Local.toast(response.data.toString());

                }

                this.updateState({ uploading_driver_license: false });

            }

        } catch (ex) {

            console.log(ex);
            Local.toast(ex.message);
            this.updateState({ uploading_driver_license: false });
        }
    }

    async uploadPhoto(image) {

        try {

            let response = await API.post('driver/upload/photo', { image: image }, this.props.api_token);

            console.log(response);

            if (response) {

                console.log(response);

                if (response.status == true) {

                    this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                    this.updateState({ photo: response.data.photo });

                } else {

                    Local.toast(response.data.toString());
                }

                this.updateState({ uploading_photo: false });

            }

        } catch (ex) {

            console.log(ex);
            Local.toast(ex.message);
            this.updateState({ uploading_photo: false });
        }
    }

    async uploadVehicleImage(image) {

        try {

            let response = await API.post('driver/upload-vehicle/image', { image: image }, this.props.api_token);

            if (response) {

                if (response.status == true) {

                    this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                    this.updateState({ vehicle_image: response.data.vehicle.image });

                } else {

                    Local.toast(response.data.toString());

                }

                this.updateState({ uploading_vehicle_image: false });

            }

        } catch (ex) {

            console.log(ex);
            Local.toast(ex.message);
            this.updateState({ uploading_vehicle_image: false });
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Join Velocity</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Driver and Vehicle Documents</Text>
                </View>

                <View style={{ margin: 0, padding: 10 }}>

                    <View style={styles.card}>

                        <Text style={styles.h1}>Driver's License</Text>
                        <Text style={styles.p}>Scanned copy of your Driver's License or a Quality Photo is accepted.</Text>

                        {this.props.uploading_driver_license
                            ? <DotIndicator color={Colors.primary} style={{ alignSelf: 'flex-end' }} size={8} />
                            : this.props.driver_license
                                ? <Text style={styles.selectTextGreen}>Upload Successful.</Text>
                                : <PhotoUpload containerStyle={{ alignSelf: 'flex-end' }} onPhotoSelect={image => this.uploadDriversLicense(image)} onResponse={() => this.updateState({ uploading_driver_license: true })}>
                                    <Text style={styles.selectText}>Select File</Text>
                                </PhotoUpload>
                        }

                    </View>

                    <View style={styles.card}>

                        <Text style={styles.h1}>Driver's Profile Photo</Text>
                        <Text style={styles.p}>Please provide a clear portrait picture (not a full body picture) of yourself. it should show your full face, front view and with eyes open.</Text>

                        {this.props.uploading_photo
                            ? <DotIndicator color={Colors.primary} style={{ alignSelf: 'flex-end' }} size={8} />
                            : this.props.photo
                                ? <Text style={styles.selectTextGreen}>Upload Successful.</Text>
                                : <PhotoUpload containerStyle={{ alignSelf: 'flex-end' }} onPhotoSelect={image => this.uploadPhoto(image)} onResponse={() => this.updateState({ uploading_photo: true })}>
                                    <Text style={styles.selectText}>Select File</Text>
                                </PhotoUpload>
                        }

                    </View>

                    <View style={styles.card}>

                        <Text style={styles.h1}>Vehicle Photo</Text>
                        <Text style={styles.p}>Please provide a clear picture of your Vehicle. </Text>

                        {this.props.uploading_vehicle_image
                            ? <DotIndicator color={Colors.primary} style={{ alignSelf: 'flex-end' }} size={8} />
                            : this.props.vehicle_image
                                ? <Text style={styles.selectTextGreen}>Upload Successful.</Text>
                                : <PhotoUpload containerStyle={{ alignSelf: 'flex-end' }} onPhotoSelect={image => this.uploadVehicleImage(image)} onResponse={() => this.updateState({ uploading_vehicle_image: true })}>
                                    <Text style={styles.selectText}>Select File</Text>
                                </PhotoUpload>
                        }

                    </View>

                    <VButton onPress={this.continue.bind(this)} text='CONTINUE' loading={this.props.loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

                <View style={styles.continue}>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.BeginDriverUpload;

const mapStateToProps = state => ({ ...state.driverUpload, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverUpload);
