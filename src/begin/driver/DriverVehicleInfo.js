import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, Alert, Image, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input } from 'native-base'
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { DRIVER_VEHICLE_INFO, AUTH } from '../../redux/types';

class DriverVehicleInfo extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_VEHICLE_INFO, payload: state });
    }

    async continue() {

        try {

            if (this.props.driver_vehicle_info_loading == true) return;

            this.updateState({ driver_vehicle_info_loading: true });

            let response = await API.post('driver/edit-vehicle', {

                make: this.props.make,
                year: this.props.year,
                model: this.props.model,
                color: this.props.color,
                plate_number: this.props.plate_number

            }, this.props.api_token);

            if (response.status == true) {

                this.props.dispatch({type : AUTH, payload : {user : response.data } });
                this.updateState({ driver_vehicle_info_loading: false });
                this.props.navigation.navigate('VerifyOTP');

            } else {

                this.updateState({ driver_vehicle_info_loading: false });
                Alert.alert('Error', response.data.toString());
            }

        } catch (ex) {

            this.updateState({ driver_vehicle_info_loading: false });
            Alert.alert('Error', ex.message);
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Join Velocity</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Enter your Vehicle Information</Text>
                </View>

                <View style={{ margin: 0, padding: 10 }}>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Vehicle Make</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ make: text })} value={this.props.make} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Vehicle Year</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ year: text })} value={this.props.year} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Vehicle Model</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ model: text })} value={this.props.model} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Vehicle Color</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ color: text })} value={this.props.color} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Vehicle Plate Number</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ plate_number: text })} value={this.props.plate_number} />
                    </Item>

                    <VButton onPress={this.continue.bind(this)} text='NEXT' loading={this.props.driver_vehicle_info_loading} background={Colors.primary} color={Colors.white}></VButton>

                </View>

                <View style={styles.continue}>
                    <Text style={{ textAlign: 'center', marginTop: 20, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.BeginDriverVehicleInfo;

const mapStateToProps = state => ({ ...state.driverVehicleInfo, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverVehicleInfo);

