import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, StyleSheet, Alert, Image, Text, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input } from 'native-base';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import API from '../../utils/API';
import Local from '../../utils/Local';
import { AUTH } from '../../redux/types';
import { USER_PERSONAL_INFO } from '../../redux/types';

class UserPersonalInfo extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_PERSONAL_INFO, payload: state });
    }

    async register() {

        try {

            if (this.props.user_profile_info_loading == true) return;
            this.updateState({ user_profile_info_loading: true });

            let response = await API.post('register', {

                phone: this.props.phone,
                first_name: this.props.first_name,
                last_name: this.props.last_name,
                email: this.props.email,
                role: 'user',
            });

            console.log(response);

            if (response.status == true) {

                this.updateState({ user_profile_info_loading: false });

                let { data } = response;
                this.props.dispatch({ type: AUTH, payload: { user: data, role: data.role } });
                this.props.navigation.navigate('VerifyOTP');

            } else {

                this.updateState({ user_profile_info_loading: false });
                Alert.alert('Error', response.data.toString());
            }

        } catch (ex) {

            this.updateState({ user_profile_info_loading: false });
            Alert.alert('Error', ex.message);
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar translucent backgroundColor={Colors.white} barStyle='dark-content' animated />

                <View style={{ alignItems: 'flex-start', paddingTop: 30, paddingLeft: 30, paddingRight: 30 }}>
                    <Image resizeMode="contain" style={{ width: 70, height: 70, marginBottom: 10 }} source={require('../../img/logo.png')} />
                    <Text style={{ color: Colors.black, textAlign: 'left', fontWeight: 'bold', fontSize: 30 }}>Join Velocity</Text>
                    <Text style={{ textAlign: 'left', marginTop: 20, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Before we begin, let us know a few things about you...</Text>
                </View>


                <View style={{ margin: 0, padding: 20 }}>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Phone Number</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ phone: text })} value={this.props.phone} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>First Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ first_name: text })} value={this.props.first_name} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Last Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ last_name: text })} value={this.props.last_name} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Email Address</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ email: text })} value={this.props.email} />
                    </Item>

                    <VButton onPress={this.register.bind(this)} text='NEXT' loading={this.props.user_profile_info_loading} background={Colors.primary} color={Colors.white}></VButton>
                </View>

                <View style={{ padding: 25, justifyContent: 'flex-end', width: '100%', }}>
                    <Text style={{ textAlign: 'center', marginTop: 10, fontWeight: 'bold' }} note>
                        By continuing, I confirm that I have read and agree to the <Text style={{ color: Colors.primary }}>terms and condition</Text> and <Text style={{ color: Colors.primary }}>privacy policy</Text>
                    </Text>
                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: Colors.white

    }
});


const mapStateToProps = state => ({ ...state.userPersonalInfo });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserPersonalInfo);


