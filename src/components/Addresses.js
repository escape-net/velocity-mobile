import React, { Component } from 'react';
import { View } from 'react-native';
import { ListItem, Body, Text, Icon } from 'native-base';

export default class Addresses extends Component {

    constructor(props) {

        super(props);
    }

    render() {

        return (

            <View>
                <ListItem style={{ margin: 0 }}>
                    <Body>
                        <Text style={{ fontSize: 16 }} numberOfLines={2}><Icon name='ios-radio-button-on' style={{ color: 'green', fontSize: 16, marginRight: 5 }} /> {this.props.pickup}</Text>
                    </Body>
                </ListItem>

                <ListItem style={{ margin: 0 }}>
                    <Body>
                        <Text style={{ fontSize: 16 }} numberOfLines={2}><Icon name='ios-radio-button-on' style={{ color: 'red', fontSize: 16, marginRight: 5 }} /> {this.props.delivery}</Text>
                    </Body>
                </ListItem>
            </View>
        )
    }
}


