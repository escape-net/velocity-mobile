import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Body, Text, Icon, Card, CardItem } from 'native-base';
import Colors from '../utils/Colors';
import Moment from 'moment';

export default class Delivery extends Component {

    constructor(props) {

        super(props);

        this.state = {

            delivery: null,
            id: null,
            reference: null,
            created_at: null,
            item: null,
            sender: null,
            recipient: null,
            pickup_address: null,
            delivery_address: null

        }
    }

    async componentWillMount() {

        let delivery = this.props.delivery;

        this.setState({

            delivery: delivery,
            id: delivery.id,
            reference: delivery.reference,
            created_at: Moment(delivery.created_at).format('MMMM Do YYYY, h:mm a'),
            item: delivery.item,
            sender: `${delivery.user.first_name} ${delivery.user.last_name}`,
            pickup_address: delivery.pickup_place.address,
            delivery_address: delivery.delivery_place.address,
            recipient: `${delivery.recipient_name} ${delivery.recipient_phone}`

        });
    }

    render() {

        return (

            <TouchableOpacity onPress={() => this.props.onPress(this.state.delivery)}>
                <Card>

                    <Text note style={{ margin: 10, marginLeft: 20 }}>
                        <Text style={{ color: Colors.black }}>#{this.state.reference}&nbsp;</Text>
                        <Text style={{ textAlign: 'right', fontSize: 10, color: '#aaa' }}>
                            {this.state.created_at}
                        </Text>
                    </Text>

                    <CardItem>

                        <Body style={{ marginTop: -20 }}>

                            <View style={{ padding: 10 }}>
                                <Text style={{ fontSize: 10, marginBottom: 5 }}>Item</Text>
                                <Text style={{ fontSize: 16 }} numberOfLines={1}>
                                    <Icon name='logo-dropbox' style={{ color: Colors.primary, fontSize: 16 }} /> {this.state.item}
                                </Text>
                            </View>

                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#ebebeb', width: '100%' }} />

                            <View style={{ padding: 10 }}>
                                <Text style={{ fontSize: 10, marginBottom: 5 }}>Sender : {this.state.sender} </Text>
                                <Text style={{ fontSize: 16 }} numberOfLines={1}><Icon name='ios-radio-button-on' style={{ color: 'green', fontSize: 16 }} /> {this.state.pickup_address}</Text>
                            </View>

                            <View style={{ borderBottomWidth: 1, borderBottomColor: '#ebebeb', width: '100%' }} />

                            <View style={{ padding: 10 }}>
                                <Text style={{ fontSize: 10, marginBottom: 5 }} >Reciepeint: {this.state.recipient}</Text>
                                <Text style={{ fontSize: 16 }} numberOfLines={1}><Icon name='ios-radio-button-on' style={{ color: 'red', fontSize: 16 }} /> {this.state.delivery_address}</Text>
                            </View>

                        </Body>

                    </CardItem>

                </Card>
            </TouchableOpacity>
        )
    }
}