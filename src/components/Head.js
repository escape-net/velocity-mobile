import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { Header, Left, Body, Right, Button, Title, Icon, Text, Badge } from 'native-base';
import Colors from '../utils/Colors';

export default class Head extends Component {

    constructor(props) {

        super(props);
    }

    render() {

        return (

            <Header style={{ backgroundColor: Colors.primary, height: 60 }} androidStatusBarColor={Colors.primaryLight}>
                <Left>
                    {this.props.back ?

                        <TouchableOpacity transparent onPress={() => { this.props.navigation.goBack(null) }}>
                            <Icon style={{ color: Colors.white }} name='arrow-back' />
                        </TouchableOpacity>

                        :

                        <TouchableOpacity transparent onPress={() => { this.props.navigation.openDrawer() }}>
                            <Icon style={{ color: Colors.white }} name='menu' />
                        </TouchableOpacity>
                    }

                </Left>
                <Body>
                    <Title style={{ color: Colors.white }}>{this.props.title ? this.props.title : 'Home'}</Title>
                </Body>

                {
                    this.props.right ?

                        <Right>

                            {this.props.badge ?

                                <View style={{ flexDirection: 'row' }}>
                                    <Icon style={styles.badgeIcon} name={this.props.rightIcon} onPress={this.props.rightPress} />
                                    <Badge style={styles.badge} onPress={this.props.rightPress}>
                                        <Text style={styles.badgeText} onPress={this.props.rightPress}>{this.props.badge}</Text>
                                    </Badge>
                                </View>
                                :
                                <TouchableOpacity onPress={this.props.rightPress} style={{ marginRight: 10 }}>
                                    <Icon style={{ color: Colors.white }} name={this.props.rightIcon} />
                                </TouchableOpacity>
                            }
                        </Right>

                        :

                        <Right />
                }

            </Header >
        )
    }
}


const styles = StyleSheet.create({

    badgeIcon: {
        color: Colors.white,
        position: 'relative',
        marginRight: 25
    },
    badge: {

        position: 'absolute',
        top: -5,
        left: 10,
        bottom: 0,
        padding: 0,
        marginRight: 15
    },
    badgeText: {
        padding: 0,
        margin: 0
    }
})