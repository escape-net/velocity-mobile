import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, View, TouchableOpacity } from 'react-native';
import { Image } from '@shoutem/ui';
import { Text, Icon } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import Colors from '../utils/Colors';
import Local from '../utils/Local';
import API from '../utils/API';
import { LOGOUT } from '../redux/types';

class UserSideMenu extends Component {

    async logout() {

        await API.get('logout', this.props.api_token);
        this.props.dispatch({ type: LOGOUT, payload: null });
        await Local.logout();
        this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Login' })], }));
    }

    render() {

        return (

            <View style={styles.container}>

                <ScrollView>

                    <View style={styles.header}>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UserProfile')}>
                            {this.props.user.photo
                                ? <Image styleName='medium-avatar' style={{ width: 50, height: 50 }} source={{ uri: this.props.user.photo }} />
                                : <Image styleName='medium-avatar' style={{ width: 50, height: 50 }} source={require('../img/avatar.png')} />
                            }
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('UserProfile')}>
                            <View style={{ marginTop: 5, marginLeft: 15 }}>
                                <Text style={{ fontSize: 18, color: Colors.white, fontWeight: 'bold' }}>{this.props.user.name}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 12, color: Colors.white }}>View Profile</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                    </View>

                    <View style={{ padding: 30 }}>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserDeliveries')} style={styles.menu}>
                                <Icon name='clock' style={styles.icon} />
                                <Text style={styles.text}>Deliveries</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserPayments')} style={styles.menu}>
                                <Icon name='cash' style={styles.icon} />
                                <Text style={styles.text}>Payments</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserProfile')} style={styles.menu}>
                                <Icon name='person' style={styles.icon} /><Text style={styles.text}>Profile</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserNotifications')} style={styles.menu}>
                                <Icon name='notifications' style={styles.icon} /><Text style={styles.text}>Notifications</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserAbout')} style={styles.menu}>
                                <Icon name='information-circle' style={styles.icon} /><Text style={styles.text}>About</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserSupport')} style={styles.menu}>
                                <Icon name='md-help-circle' style={styles.icon} /><Text style={styles.text}>Support</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ paddingBottom: 30 }}>
                            <TouchableOpacity onPress={this.logout.bind(this)} style={styles.menu}>
                                <Icon name='log-out' style={styles.icon} /><Text style={styles.text}>Logout</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <Text style={{ color: Colors.white, textAlign: 'right', alignSelf: 'flex-end' }}>V 2.0.0</Text>
                </View>

            </View >
        )
    }
}

const styles = StyleSheet.create({

    container: {

        backgroundColor: Colors.white,
        width: '100%',
        height: '100%',
        flex: 1
    },
    menu: {

        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    icon: {

        color: Colors.primary,
        fontSize: 25, paddingRight: 15
    },
    text: {

        fontSize: 18,
        color: Colors.black
    },
    view: {
        paddingBottom: 30,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    header: {
        flexDirection: 'row',
        paddingTop: 50,
        paddingBottom: 30,
        paddingLeft: 30,
        paddingRight: 30,
        backgroundColor: Colors.primary,
        height: 140
    },
    footer: {
        backgroundColor: Colors.primary,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
});


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserSideMenu);
