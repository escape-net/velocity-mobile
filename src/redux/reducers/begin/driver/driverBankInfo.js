import { DRIVER_BANK_INFO, LOGOUT } from '../../../types';

const initialState = {

    driver_bank_info_loading: false,
    bank_name: '',
    bank_account_number: '',
    bank_account_name: '',
    email: '',
    banks: null,
    bank_id: null
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_BANK_INFO:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}