import { DRIVER_CATEGORY, LOGOUT } from '../../../types';

const initialState = {

    driver_category_loading: false,
    category: 1
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_CATEGORY:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}