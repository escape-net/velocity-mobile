import { DRIVER_PERSONAL_INFO, LOGOUT } from '../../../types';

const initialState = {

    driver_personal_info_loading: false,
    phone: '',
    first_name: '',
    last_name: '',
    email: '',
    role: 'driver',
    category: 1,
    vehicle_types: null,
    vehicle_type_id: 0
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_PERSONAL_INFO:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}