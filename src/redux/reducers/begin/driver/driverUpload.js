import { DRIVER_UPLOAD, LOGOUT } from '../../../types';

const initialState = {

    api_token: null,
    driver_license: null,
    uploading_driver_license: false,
    photo: null,
    uploading_photo: false,
    vehicle_image: null,
    uploading_vehicle_image: false
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_UPLOAD:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}