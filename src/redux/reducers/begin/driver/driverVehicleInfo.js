import { DRIVER_VEHICLE_INFO, LOGOUT } from '../../../types';

const initialState = {

    driver_vehicle_info_loading: false,
    api_token: '',
    make: '',
    year: '',
    model: '',
    color: '',
    plate_number: ''
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_VEHICLE_INFO:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}