import { USER_PERSONAL_INFO, LOGOUT } from '../../../types';

const initialState = {

    user_personan_info_loading: false,
    phone: '',
    first_name: '',
    last_name: '',
    email: '',
    role: 'user',
    category: 1
};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_PERSONAL_INFO:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}