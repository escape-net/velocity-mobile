import { DRIVER_ACCEPT_DELIVERY, LOGOUT } from '../../types';

const initialState = {

    accept_status: false,
    delivery: null,
    sender: null,
    item: null,
    pickup_address: null,
    delivery_address: null,
    accept_status: false,
    decline_status: false

};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_ACCEPT_DELIVERY:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}