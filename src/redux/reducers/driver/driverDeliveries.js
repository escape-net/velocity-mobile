import { DRIVER_DELIVERIES, LOGOUT } from '../../types';

const initialState = { completed: [], assigned: [], active: [] };

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_DELIVERIES:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}