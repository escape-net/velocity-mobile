import { DRIVER_HOME, LOGOUT } from '../../types';

const initialState = {

    driver_status: false,
    position: null,
    marker: null,
    upcoming_count: null,
    right_text: null

};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_HOME:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}