import { DRIVER_PROFILE, LOGOUT } from '../../types';

const initialState = {

    loading: false,
    photo: null,
    uploading: false,
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    vehicle: '',
    plate_number: '',
    edit: false

};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_PROFILE:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}