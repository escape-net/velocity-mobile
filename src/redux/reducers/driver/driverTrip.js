import { DRIVER_TRIP, LOGOUT } from '../../types';

const initialState = {

    trip_title: 'Trip',
    delivery: null,
    pickup_place: null,
    delivery_place: '',
    directions: false,
    vehicle_image: null,
    item: null,
    recipient: '',
    start: null,
    on_trip: null,
    pickup_status: false,
    arrived_status: false,
    phone: ''

};

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case DRIVER_TRIP:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}