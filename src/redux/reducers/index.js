import { combineReducers } from 'redux';

import auth from './auth';

//Begin Driver
import driverBankInfo from './begin/driver/driverBankInfo';
import driverCategory from './begin/driver/driverCategory';
import driverPersonalInfo from './begin/driver/driverPersonalInfo';
import driverUpload from './begin/driver/driverUpload';
import driverVehicleInfo from './begin/driver/driverVehicleInfo';

//Begin User
import userPersonalInfo from './begin/user/userPersonalInfo';


//User
import userHome from './user/userHome';
import userDeliveryDetails from './user/userDeliveryDetails';
import userTrip from './user/userTrip';
import userRateDriver from './user/userRateDriver';
import userDeliveries from './user/userDeliveries';
import userPayments from './user/userPayments';
import userProfile from './user/userProfile';

//Driver
import driverHome from './driver/driverHome';
import driverAcceptDelivery from './driver/driverAcceptDelivery';
import driverProfile from './driver/driverProfile';
import driverDeliveries from './driver/driverDeliveries';
import driverTrip from './driver/driverTrip';

export default combineReducers({

    auth,

    driverBankInfo,
    driverCategory,
    driverPersonalInfo,
    driverUpload,
    driverVehicleInfo,

    userPersonalInfo,

    userHome,
    userDeliveryDetails,
    userTrip,
    userRateDriver,
    userDeliveries,
    userPayments,
    userProfile,

    driverHome,
    driverAcceptDelivery,
    driverProfile,
    driverDeliveries,
    driverTrip

})