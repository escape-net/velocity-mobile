import { NOTIFICATIONS, LOGOUT } from '../types';

const initialState = {

    notifications: null
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case NOTIFICATIONS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}