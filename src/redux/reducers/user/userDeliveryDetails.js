import { USER_DELIVERY_DETAILS, LOGOUT } from '../../types';

const initialState = {

    loading: false,
    estimate: null,
    estimate_amount: null,
    item: null,
    recipient_name: null,
    recipient_phone: null,
    instructions: null
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_DELIVERY_DETAILS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}