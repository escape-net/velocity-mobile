import { USER_HOME, LOGOUT } from '../../types';

const initialState = {

    delivery_mode: 'Select Delivery Mode',
    vehicle_type_id: 1,
    vehicle_types: {},
    root: { latitude: 6.5244, longitude: 3.3792, longitudeDelta: 0.03, latitudeDelta: 0.03 },
    pickup: null,
    delivery: null,
    pickup_address: null,
    delivery_address: null,
    directions: false,
    payment: null,
    places: []

}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_HOME:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}