import { USER_PAYMENTS, LOGOUT } from '../../types';

const initialState = {

    cards: []
}
export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_PAYMENTS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}