import { USER_PROFILE, LOGOUT } from '../../types';

const initialState = {

    profile_loading: false,
    photo: null,
    uploading: false,
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    edit: false

}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_PROFILE:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}