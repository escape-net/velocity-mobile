import { USER_RATE_DRIVER, LOGOUT } from '../../types';

const initialState = {

    rate_loading: false,
    rate: 0,
    comment: ''
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_RATE_DRIVER:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}