import { USER_TRIP, LOGOUT } from '../../types';

const initialState = {

    trip_title: 'Trip',
    delivery: {},
    pickup_place: null,
    delivery_place: null,
    driver: null,
    driver_location: null,
    driver_accepted: null,
    driver_location: null,
    searching: true,
    search: false,
    started: false,
    pickup_status: false

}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case USER_TRIP:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        case LOGOUT:
            return { ...state, ...initialState };

        default:
            return state;
    }
}