import React, { Component } from 'react';
import { Linking, View, } from 'react-native';
import { Icon, Text, Left, List, ListItem, Right } from 'native-base';
import Head from '../components/Head';
import Colors from '../utils/Colors';

export default class About extends Component {

  render() {

    return (

      <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

        <Head navigation={this.props.navigation} title='About Velocity' back={true} />

        <View style={{ marginLeft: 10, marginTop: 20 }}>
          <List style={{ marginTop: 10 }}>
            <ListItem onPress={() => { Linking.openURL('http://velocity.ng') }}>
              <Left>
                <Text note>Velocity</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => { Linking.openURL('http://velocity.ng/shipping-policy.pdf') }}>
              <Left>
                <Text note>Shipping Policy</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => { Linking.openURL('http://velocity.ng/privacy-policy.pdf') }}>
              <Left>
                <Text note>Privacy Policy</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => { Linking.openURL('http://velocity.ng/#about') }}>
              <Left>
                <Text note>FAQ</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>

            <ListItem onPress={() => { Linking.openURL('http://velocity.ng') }}>
              <Left>
                <Text note>Contact Us</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </View>
      </View>
    )
  }
}
