import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Body, List, ListItem, Text } from 'native-base';
import Head from '../components/Head';
import Colors from '../utils/Colors';
import API from '../utils/API';
import Local from '../utils/Local';
import Moment from 'moment';
import { NOTIFICATIONS } from '../redux/types';

class Notifications extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: NOTIFICATIONS, payload: state });
    }

    async componentDidMount() {

        try {

            let response = await API.get('notifications', this.props.api_token);

            if (response.status == true)
                this.updateState({ notifications: response.data });

        } catch (ex) {

            Local.toast('Could not get your Notifications at this time.');
        }

    }

    render() {

        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Head navigation={this.props.navigation} title='Notifications' back={true} />

                <List style={{ marginTop: 10 }}>

                    {this.props.notifications
                        ? this.props.notifcations.map(row => (

                            <ListItem key={row.id}>
                                <Body>
                                    {row.subject
                                        ? <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>{row.subject}</Text>
                                        : null}

                                    <Text>{row.body}</Text>
                                    <Text note>{Moment(row.created_at).format('MMMM Do YYYY, h:mm a')}</Text>
                                </Body>
                            </ListItem>
                        ))
                        : <Text style={{ marginTop: '50%', textAlign: 'center', color: Colors.black }}>Notifications concerning your account will be avaliable here once you have any.</Text>}

                </List>

            </View>
        )
    }
}


const mapStateToProps = state => ({ ...state.notifications, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);


