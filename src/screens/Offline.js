import React, { Component } from 'react';
import { View, TouchableOpacity, Alert, StatusBar } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Text } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import Colors from '../utils/Colors';
import Local from '../utils/Local';

export default class Offline extends Component {

    constructor(props) {

        super(props);
        this.state = { role: null };
    }

    async componentDidMount() {

        try {

            let user = await Local.get('user');
            if (user) this.setState({ role: user.role });

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    retry = async () => {

        try {

            let network = await NetInfo.getConnectionInfo();

            if (network.type != 'none') {

                let route = 'Auth';
                if (this.state.role == 'driver') route = 'DriverMenu';
                if (this.state.role == 'user') route = 'UserMenu';

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));

            } else {

                Alert.alert('Internet Connectivity', 'Please check your network and try again.');
            }

        } catch (ex) {

            Local.toast(ex.message);
        }

    }
    render() {

        return (

            <View style={{ backgroundColor: Colors.primary, width: '100%', height: '100%' }}>

                <StatusBar translucent backgroundColor={Colors.primary} barStyle='light-content' animated />

                <View style={{ flexDirection: 'column', padding: 30 }}>
                    <Text style={{ marginTop: '50%', marginBottom: 40, textAlign: 'center', color: Colors.white, fontSize: 25 }}>Can't connect to internet. Please check your network settings!</Text>

                    <TouchableOpacity onPress={() => this.retry()} style={{ backgroundColor: Colors.white, width: '50%', alignSelf: 'center', borderRadius: 50, padding: 0 }}>
                        <View>
                            <Text style={{ color: Colors.primary, textAlign: 'center', padding: 10, fontSize: 18 }}>Try Again</Text>
                        </View>

                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}
