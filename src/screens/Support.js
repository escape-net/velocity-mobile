import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Text, H1 } from 'native-base';
import { Image } from '@shoutem/ui';
import Head from '../components/Head';
import Colors from '../utils/Colors';
import Styles from '../utils/Styles';

export default class Support extends Component {

    constructor(props) {

        super(props);
    }


    render() {

        return (

            <ScrollView style={styles.container}>

                <Head navigation={this.props.navigation} title='Support' back={true} />

                <View style={{ backgroundColor: Colors.white, height: '100%' }}>


                    <H1 style={{ marginTop: 50, fontSize: 30, textAlign: 'center', color: Colors.black }}>You Can Reach us Anytime!</H1>

                    <Image source={require('../img/contact.png')} style={{ width: 400, height: 400 }} />

                    <Text style={{ textAlign: 'center' }}>our email is always open</Text>
                    <Text style={{ textAlign: 'center', fontSize: 18, color: Colors.primary }}>hello@velocity.ng</Text>

                    <Text style={{ textAlign: 'center', marginTop: 20 }}>Give us a Call (Mon-Fri 9am-5pm)</Text>
                    <Text style={{ textAlign: 'center', fontSize: 18, color: Colors.primary }}>+234 807 766 6455</Text>


                </View>

            </ScrollView>
        )
    }
}

const styles = Styles.Support;