import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { ListItem, Text, Body, Icon, Left, Thumbnail, Separator, Right } from 'native-base';
import Head from '../components/Head';
import Addresses from '../components/Addresses';
import Moment from 'moment';
import Colors from '../utils/Colors';
import Styles from '../utils/Styles';

class TripDetail extends Component {

    constructor(props) {

        super(props);

        this.state = {

            delivery: null,
            id: null,
            reference: null,
            created_at: null,
            item: null,
            sender: null,
            recipient: null,
            pickup_address: null,
            delivery_address: null,
            total: null,
            commission: null,
            instructions: null,
            start_at: null,
            end_at: null,
            is_driver: null

        }
    }

    async componentWillMount() {

        let delivery = this.props.navigation.getParam('delivery');

        let user_type = this.props.role;
        if (user_type == 'driver') this.setState({ is_driver: true });

        this.setState({

            delivery: delivery,
            id: delivery.id,
            reference: delivery.reference,
            created_at: Moment(delivery.created_at).format('MMMM Do YYYY, h:mm a'),
            item: delivery.item,
            sender: `${delivery.user.first_name} ${delivery.user.last_name}`,
            pickup_address: delivery.pickup_place.address,
            delivery_address: delivery.delivery_place.address,
            recipient: `${delivery.recipient_name} ${delivery.recipient_phone}`,
            total: delivery.total,
            commission: delivery.commission,
            instructions: delivery.instructions,
            start_at: delivery.start_at,
            end_at: delivery.end_at

        });
    }

    render() {

        return (

            <ScrollView style={styles.container}>

                <Head navigation={this.props.navigation} title='Trip Detail' back={true} />

                <View style={{ backgroundColor: Colors.white }}>

                    <Addresses pickup={this.state.pickup_address} delivery={this.state.delivery_address} />

                    <ListItem avatar>
                        <Left>
                            <Thumbnail source={require('../img/avatar.png')} />
                        </Left>
                        <Body style={{ borderBottomWidth: 0 }}>
                            <Text>Delivery made for {this.state.sender}</Text>
                            <Text note>{this.state.created_at}</Text>
                        </Body>
                    </ListItem>

                    <View style={{ marginTop: 20 }}>
                        <Separator bordered>
                            <Text style={{ fontSize: 15 }}>Item Details</Text>
                        </Separator>
                    </View>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Reference</Text>
                            <Text>#{this.state.reference}</Text>
                        </Body>
                    </ListItem>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Item</Text>
                            <Text>{this.state.item}</Text>
                        </Body>
                    </ListItem>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Recipient</Text>
                            <Text>{this.state.recipient}</Text>
                        </Body>
                    </ListItem>

                    <ListItem style={{ margin: 0, borderBottomWidth: 0 }}>
                        <Body>
                            <Text note>Delivery Instruction</Text>
                            <Text>{this.state.instructions}</Text>
                        </Body>
                    </ListItem>


                    <View style={{ marginTop: 20 }}>
                        <Separator bordered>
                            <Text style={{ fontSize: 15 }}>Payment Details</Text>
                        </Separator>
                    </View>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Payment Status</Text>
                            <Text>PAID</Text>
                        </Body>
                    </ListItem>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Cash</Text>
                            <Text style={{ fontSize: 18 }}>{this.state.total}</Text>
                        </Body>
                    </ListItem>

                    {this.state.is_driver
                        ? <ListItem style={{ margin: 0, borderBottomWidth: 0 }}>
                            <Body>
                                <Text note>Your Commission</Text>
                                <Text style={{ fontSize: 18 }}>{this.state.commission}</Text>
                            </Body>
                        </ListItem>
                        : null}



                    <View style={{ marginTop: 20 }}>
                        <Separator bordered>
                            <Text style={{ fontSize: 15 }}>Trip Details</Text>
                        </Separator>
                    </View>

                    <ListItem style={{ margin: 0 }}>
                        <Body>
                            <Text note>Started At</Text>
                            <Text>{this.state.start_at}</Text>
                        </Body>
                    </ListItem>

                    <ListItem style={{ margin: 0, borderBottomWidth: 0 }}>
                        <Body>
                            <Text note>Ended At</Text>
                            <Text>{this.state.end_at}</Text>
                        </Body>
                    </ListItem>

                    <View style={{ marginTop: 20 }}>
                        <Separator bordered>
                            <Text style={{ fontSize: 15 }}>Have a question ?</Text>
                        </Separator>
                    </View>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DriverSupport')}>
                        <ListItem style={{ margin: 0 }}>
                            <Body>
                                <Text>Contact Support</Text>
                            </Body>
                            <Right>
                                <Icon name='ios-arrow-forward' />
                            </Right>
                        </ListItem>
                    </TouchableOpacity>


                </View>

            </ScrollView>
        )
    }
}

const styles = Styles.TripDetail;

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(TripDetail);


