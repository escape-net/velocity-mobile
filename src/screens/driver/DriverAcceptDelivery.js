import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Vibration, Alert, Dimensions } from 'react-native';
import { Text, Header, Left, Button, Right, Icon, ListItem, Body } from 'native-base';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import * as Animatable from 'react-native-animatable';
import MapView from 'react-native-maps';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { DRIVER_ACCEPT_DELIVERY, AUTH } from '../../redux/types';

class DriverAcceptDelivery extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_ACCEPT_DELIVERY, payload: state });
    }

    async componentDidMount() {

        try {

            let { delivery } = this.props;

            this.updateState({

                sender: `${delivery.user.first_name} ${delivery.user.last_name}`,
                pickup_address: delivery.pickup_place.address,
                delivery_address: delivery.delivery_place.address

            });

            if (!this.props.map_delta) {

                let { width, height } = Dimensions.get('window');
                let map_delta = Local.googleMapsDelta(width, height);
                this.props.dispatch({ type: AUTH, payload: { map_delta } });
            }

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    decline = async () => {

        try {

            Vibration.cancel();
            this.updateState({ decline_status: false });

            let response = await API.get(`driver/decline-delivery/${this.props.delivery.id}`, this.props.api_token);

            Alert.alert('', response.data);

            if (response.status == true)
                this.props.navigation.goBack();

        } catch (ex) {

            this.updateState({ decline_status: false });
            Local.toast(ex.message);
        }
    }

    accept = async () => {

        try {

            Vibration.cancel();
            this.updateState({ accept_status: false });
            this.props.navigation.navigate('DriverTrip', { id: this.props.delivery.id });

        } catch (ex) {

            this.updateState({ accept_status: false });
            Local.toast(ex.message);
        }
    }

    render() {

        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Header style={{ backgroundColor: Colors.primary, height: 60 }} androidStatusBarColor={Colors.primaryLight}>

                    <Left>
                        <Button rounded danger iconLeft small onPress={() => this.updateState({ decline_status: true })}>
                            <Icon name='ios-close-circle' style={{ color: Colors.white }} />
                            <Text style={{ color: Colors.white }}>DECLINE</Text>
                        </Button>
                    </Left>

                    <Right>
                        <Button rounded iconLeft small style={{ backgroundColor: Colors.white }} onPress={() => this.updateState({ accept_status: true })}>
                            <Icon name='ios-checkmark-circle' style={{ color: Colors.primary }} />
                            <Text style={{ color: Colors.primary }}>ACCEPT</Text>
                        </Button>
                    </Right>

                </Header>

                <View style={styles.container}>

                    <MapView style={styles.map} region={this.props.position} showsMyLocationButton={true} ref={ref => { this.map = ref }} customMapStyle={Local.mapCustomStyle()}></MapView>

                    <Animatable.View animation='pulse' easing='ease-out' iterationCount='infinite' style={styles.sender}>

                        <ListItem style={{ margin: 0, borderBottomColor: Colors.white }}>
                            <Body style={{ borderBottomWidth: 0 }}>
                                <Text style={{ fontSize: 16, color: Colors.white }}>Sender : {this.props.sender}</Text>
                            </Body>
                        </ListItem>

                        <ListItem style={{ margin: 0, borderBottomColor: Colors.white }}>
                            <Body style={{ borderBottomWidth: 0 }}>
                                <Text style={{ fontSize: 16, color: Colors.white }}>Item : {this.props.delivery.item}</Text>
                            </Body>
                        </ListItem>

                        <ListItem style={{ margin: 0, borderBottomColor: Colors.white }}>
                            <Body>
                                <Text style={{ fontSize: 16, color: Colors.white }} numberOfLines={2}><Icon name='ios-radio-button-on' style={{ color: 'green', fontSize: 16, marginRight: 5 }} /> {this.props.pickup_address}</Text>
                            </Body>
                        </ListItem>

                        <ListItem style={{ margin: 0, borderBottomWidth: 0 }}>
                            <Body>
                                <Text style={{ fontSize: 16, color: Colors.white }} numberOfLines={2}><Icon name='ios-radio-button-on' style={{ color: 'red', fontSize: 16, marginRight: 5 }} /> {this.props.delivery_address}</Text>
                            </Body>
                        </ListItem>

                    </Animatable.View>
                </View>

                <ConfirmDialog title='Accept Delivery' message='Are you sure you want to Accept this Delivery?' visible={this.props.accept_status} onTouchOutside={() => this.updateState({ accept_status: false })} positiveButton={{ title: 'Accept', onPress: () => this.accept() }} negativeButton={{ title: 'Close', onPress: () => this.updateState({ accept_status: false }) }} />
                <ConfirmDialog title='Decline Delivery' message='Are you sure you want to Decline this Delivery?' visible={this.props.decline_status} onTouchOutside={() => this.updateState({ decline_status: false })} positiveButton={{ title: 'Decline', onPress: () => this.decline() }} negativeButton={{ title: 'Close', onPress: () => this.updateState({ decline_status: false }) }} />

            </View>
        );
    }
}

const styles = Styles.DriverAcceptDelivery;

const mapStateToProps = state => ({ ...state.driverAcceptDelivery, ...state.driverHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverAcceptDelivery);