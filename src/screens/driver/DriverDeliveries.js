import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, ImageBackground } from 'react-native';
import { Tabs, Tab, Text } from 'native-base';
import Head from '../../components/Head';
import Delivery from '../../components/Delivery';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { DRIVER_DELIVERIES, DRIVER_TRIP } from '../../redux/types';

class DriverDeliveries extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_DELIVERIES, payload: state });
    }

    async componentDidMount() {

        try {

            let response = await API.get('driver/deliveries', this.props.api_token);

            if (response.status == true) {

                this.updateState({ assigned: response.data.assigned, active: response.data.active });

            }

        } catch (ex) {

            Local.toast('Could not get your deliveries at this time.');
        }

    }

    renderDelivery = data => {

        return (<Delivery delivery={data} key={data.id} onPress={this.trip} />);
    }

    trip = data => {

        this.props.dispatch({ type: DRIVER_TRIP, payload: { delivery: data } });
        this.props.navigation.navigate('DriverTrip');
    }

    render() {

        let assigned = this.props.assigned.map((row, index) => this.renderDelivery(row));
        let active = this.props.active.map((row, index) => this.renderDelivery(row));

        return (

            <View style={styles.container}>

                <Head navigation={this.props.navigation} title='Deliveries' back={true} />

                <Tabs>

                    <Tab heading='Upcoming' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: Colors.black }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'bold' }}>

                        {this.props.assigned.length < 1
                            ? <ImageBackground source={require('../../img/deliveries.png')} style={styles.background}>
                                <Text style={styles.info}>When deliveries are assigned to you, they will show up here. For now, you have no upcoming Deliveries.</Text>
                            </ImageBackground>
                            : <View style={{ padding: 10 }}>
                                <ScrollView>{assigned}</ScrollView>
                            </View>
                        }

                    </Tab>

                    <Tab heading='Active' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: Colors.black }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'bold' }}>

                        {this.props.active.length < 1
                            ? <ImageBackground source={require('../../img/deliveries.png')} style={styles.background}>
                                <Text style={styles.info}>Whenever you start a Delivery and you are yet to complete it, it stays as an active delivery and you will find them here. For now you have no Started delivery that is yet to be completed.</Text>
                            </ImageBackground>
                            : <View style={{ padding: 10 }}>
                                <ScrollView>{active}</ScrollView>
                            </View>
                        }

                    </Tab>

                </Tabs>

            </View>
        )
    }
}

const styles = Styles.DriverDeliveries;


const mapStateToProps = state => ({ ...state.driverDeliveries, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverDeliveries);
