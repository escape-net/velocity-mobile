import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Switch, Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Text, Footer, Thumbnail } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import MapView, { Marker, PROVIDER_GOOGLE, Circle } from 'react-native-maps';
import Head from '../../components/Head';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { DRIVER_HOME, DRIVER_DELIVERIES, AUTH } from '../../redux/types';
import { throwStatement } from '@babel/types';

class DriverHome extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_HOME, payload: state });
    }

    async componentDidMount() {

        try {

            let network = await NetInfo.getConnectionInfo();

            if (network.type == 'none') {

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Offline' })] }));
                return;
            }

            this.updateState({ status: this.props.user.driver_status == 'offline' ? false : true });

            let location = await Local.getCurrentPosition();

            let { latitude, longitude } = location;

            let { width, height } = Dimensions.get('window');
            let map_delta = Local.googleMapsDelta(width, height);
            this.props.dispatch({ type: AUTH, payload: { map_delta } });

            if (location.error) {

                //Local.toast(location.error);

            } else {

                let place = await Local.getGooglePlace(latitude, longitude, width, height);

                if (place.latitude && place.longitude && this.props.map_delta) {

                    this.updateState({

                        position: { latitude: latitude, longitude: longitude, ...this.props.map_delta },
                        marker: { latitude: latitude, longitude: longitude, ...this.props.map_delta }
                    });

                    await API.get(`update-location/${place.latitude}/${place.longitude}`, this.props.api_token);
                }
            }

            if (latitude && longitude && this.map)
                setTimeout(() => this.map.fitToElements(true));

            let deliveries = await API.get('driver/deliveries', this.props.api_token);
            this.props.dispatch({ type: DRIVER_DELIVERIES, payload: { deliveries: deliveries.data } });

            // if (deliveries.status == true) {

            //     let count = deliveries.data.assigned ? deliveries.data.assigned.length : 0;
            //     this.updateState({ upcoming_count: count });

            // } else {

            //     this.updateState({ right_text: null });
            // }

            this.watchId = navigator.geolocation.watchPosition(async position => {

                let { latitude, longitude } = position.coords;
                await API.get(`update-location/${latitude}/${longitude}`, this.props.api_token);

            }, error => { },

                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 }
            );

        } catch (ex) {

            //Local.toast(ex.message);
        }
    }

    componentWillUnmount() {

        try {

            navigator.geolocation.clearWatch(this.watchId);

        } catch (ex) {

            Local.toast(ex);
        }
    }

    changeStatus = async () => {

        try {

            let newStatus = this.props.driver_status == true ? 'offline' : 'online';

            this.updateState({ driver_status: !this.props.driver_status });

            let response = await API.get(`driver/status/${newStatus}`, this.props.api_token);

            Local.toast(response.data.toString());

        } catch (ex) {

            Local.toast(ex.message.toString());
        }
    }

    render() {

        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Head navigation={this.props.navigation} right={this.props.upcoming_count > 0 ? true : false} rightPress={() => this.props.navigation.navigate('DriverDeliveries')} rightIcon='md-git-branch' badge={this.props.upcoming_count} />

                <View style={styles.container}>

                    {this.props.position
                        ? <MapView provider={PROVIDER_GOOGLE} style={styles.map} region={this.props.position} maxZoomLevel={15} showsMyLocationButton={true} ref={ref => { this.map = ref }} customMapStyle={Local.mapCustomStyle()}>


                            {/* {this.props.marker
                                ? <Circle center={this.props.position} radius={70} strokeWidth={1} strokeColor={Colors.primary} fillColor='rgba(82, 0, 159, 0.5)' />
                                : null} */}

                            {this.props.marker
                                ? <Marker draggable coordinate={this.props.marker} description={'Pickup Location'} pinColor={Colors.primary} />
                                : null
                            }

                        </MapView>
                        : null}


                </View>

                <Footer style={styles.footer}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        {this.props.user.vehicle
                            ? <Thumbnail source={{ uri: this.props.user.vehicle.image }} />
                            : <Thumbnail source={require('../../img/logo.png')} />
                        }

                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>
                            <Text style={{ paddingLeft: 10, fontWeight: 'bold', color: Colors.black, fontSize: 20, flexWrap: 'wrap', width: 320 }}>{this.props.user.name}</Text>
                            {this.props.user.vehicle
                                ? <Text style={{ paddingLeft: 10, color: '#aaa', fontSize: 12 }}>Driver / {this.props.user.vehicle.plate_number}</Text>
                                : null}

                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>
                            <Text style={{ color: this.props.driver_status == true ? Colors.green : '#aaa', textAlign: 'center', marginRight: 10 }}>
                                {this.props.driver_status == true ? 'You are Online' : 'Go Online'}
                            </Text>
                            <Switch onValueChange={this.changeStatus} value={this.props.driver_status ? true : false} style={{ transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }], marginRight: 30 }} />
                        </View>
                    </View>

                </Footer>

            </View>
        );
    }
}

const styles = Styles.DriverHome;

const mapStateToProps = state => ({ ...state.driverHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverHome);