import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Text, Thumbnail, Item, Label, Input, List, ListItem, Left, Right, Icon } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import VButton from '../../components/VButton';
import PhotoUpload from 'react-native-photo-upload';
import { SkypeIndicator } from 'react-native-indicators';
import Head from '../../components/Head';
import Local from '../../utils/Local';
import API from '../../utils/API';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { AUTH, DRIVER_PROFILE } from '../../redux/types';

class DriverProfile extends Component {

  updateState = (state = {}) => {

    this.props.dispatch({ type: DRIVER_PROFILE, payload: state });
  }

  async componentDidMount() {

    try {

      this.updateState({

        first_name: this.props.user.first_name,
        last_name: this.props.user.last_name,
        phone: this.props.user.phone,
        email: this.props.user.emailr

      });

      let { vehicle } = this.props.user;

      if (vehicle) {

        if (vehicle.plate_number) this.updateState({ plate_number: vehicle.plate_number });

        if (vehicle.make && vehicle.model && vehicle.year && vehicle.color) {

          let { make, model, year, color } = vehicle;
          this.updateState({ vehicle: `${make}/${model}/${year}/${color}` });
        }

      }

    } catch (ex) {

    }
  }

  update() {

    this.updateState({ loading: true });

  }

  async edit() {

    try {

      this.updateState({ loading: true });

      let response = await API.post('driver/edit-profile', {

        first_name: this.props.first_name,
        last_name: this.props.last_name,
        email: this.props.email,
        phone: this.props.phone

      }, this.props.api_token);

      if (response.status == true) {

        this.props.dispatch({ type: AUTH, payload: { user: response.data } });
        this.updateState({ edit: false, loading: false });

      } else {

        Local.toast(response.data.toString());
        this.updateState({ loading: false });

      }

    } catch (ex) {

      Local.toast(ex.message);
      this.updateState({ loading: false });

    }
  }

  async upload(image) {

    try {

      let response = await API.post('driver/upload/photo', { image: image });

      if (response) {

        if (response.status == true) {

          this.props.dispatch({ type: AUTH, payload: { user: response.data } });
          this.updateState({ photo: response.data.photo });

        } else {

          Local.toast(response.data.toString());

        }

        setTimeout(() => this.updateState({ uploading: false }), 1000);

      }

    } catch (ex) {

      this.updateState({ uploading: false });
    }


  }

  render() {

    return (

      <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

        <Head navigation={this.props.navigation} title='Profile' back={true} right={true} rightPress={() => this.updateState({ edit: !this.props.edit })} rightIcon='md-create' />

        <View style={styles.header}>

          <PhotoUpload onPhotoSelect={image => this.upload(image)} onResponse={() => this.updateState({ uploading: true })}>
            {this.props.uploading
              ? <SkypeIndicator color={Colors.white} />
              : this.props.user.photo
                ? <Thumbnail large source={{ uri: this.props.user.photo }} style={styles.thumbnail} />
                : <Thumbnail large source={require('../../img/avatar.png')} style={styles.thumbnail} />}

          </PhotoUpload>

        </View>

        {this.props.edit
          ? <Animatable.View animation='fadeIn' easing='ease-out' iterationCount={1} style={{ margin: 0, padding: 10 }}>

            <Item floatingLabel style={{ marginBottom: 30, marginTop: 30 }}>
              <Label style={{ marginTop: 5 }}>Phone Number</Label>
              <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ phone: text })} value={this.props.phone} />
            </Item>

            <Item floatingLabel style={{ marginBottom: 30 }}>
              <Label style={{ marginTop: 5 }}>First Name</Label>
              <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ first_name: text })} value={this.props.first_name} />
            </Item>

            <Item floatingLabel style={{ marginBottom: 30 }}>
              <Label style={{ marginTop: 5 }}>Last Name</Label>
              <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ last_name: text })} value={this.props.last_name} />
            </Item>

            <Item floatingLabel style={{ marginBottom: 30 }}>
              <Label style={{ marginTop: 5 }}>Email Address</Label>
              <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ email: text })} value={this.props.email} />
            </Item>

            <VButton onPress={this.edit.bind(this)} text='SAVE' loading={this.props.loading} background={Colors.primary} color={Colors.white}></VButton>
          </Animatable.View>
          : <Animatable.View animation='fadeIn' easing='ease-out' iterationCount={1} style={{ padding: 10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 30 }}>
              <View>

                <Text style={styles.h1}>First Name</Text>
                <Text style={styles.p}>{this.props.first_name}</Text>

                <Text style={styles.h1}>Phone</Text>
                <Text style={styles.p}>{this.props.phone}</Text>

                <Text style={styles.h1}>Vehicle Description</Text>
                <Text style={styles.p}>{this.props.vehicle}</Text>

                <Text style={styles.h1}>Plate Number</Text>
                <Text style={styles.p}>{this.props.plate_number}</Text>

              </View>

              <View>

                <Text style={styles.h1}>Last Name</Text>
                <Text style={styles.p}>{this.props.last_name}</Text>

                <Text style={styles.h1}>Email</Text>
                <Text style={styles.p}>{this.props.email}</Text>

              </View>

            </View>

            <List style={{ marginTop: 10 }}>
              <ListItem onPress={() => this.props.navigation.navigate('DriverBankInfo')}>
                <Left>
                  <Text>Bank Details</Text>
                </Left>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>
              <ListItem onPress={() => { }}>
                <Left>
                  <Text>Manage Vehicle</Text>
                </Left>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>
              <ListItem onPress={() => { }}>
                <Left>
                  <Text>Vehicle Documents</Text>
                </Left>
                <Right>
                  <Icon name='arrow-forward' />
                </Right>
              </ListItem>
            </List>

          </Animatable.View>
        }

      </KeyboardAwareScrollView>
    )
  }
}

const styles = Styles.DriverProfile;


const mapStateToProps = state => ({ ...state.driverProfile, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverProfile);
