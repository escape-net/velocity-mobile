import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Linking, Alert, Dimensions } from 'react-native';
import { Text, Footer, Thumbnail, Icon } from 'native-base';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import * as Animatable from 'react-native-animatable';
import { Button } from '@shoutem/ui';
import Head from '../../components/Head';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { DRIVER_TRIP, AUTH } from '../../redux/types';

const GOOGLE_MAPS_APIKEY = Local.googleMapsKey();

class DriverTrip extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_TRIP, payload: state });
    }

    async componentWillMount() {

        try {

            let { user } = this.props;

            if (user.vehicle && user.vehicle.image)
                this.updateState({ vehicle_image: user.vehicle.image });

            let response = await API.get(`delivery/${this.props.delivery.id}`, this.props.api_token);

            let { width, height } = Dimensions.get('window');
            let map_delta = Local.googleMapsDelta(width, height);
            this.props.dispatch({ type: AUTH, payload: { map_delta } });

            if (response.status == false) {

                return this.props.navigation.navigate('DriverHome');

            } else {

                let delivery = response.data;

                let pickup_place = { latitude: parseFloat(delivery.pickup_place.latitude), longitude: parseFloat(delivery.pickup_place.longitude), ...this.props.map_delta };
                let delivery_place = { latitude: parseFloat(delivery.delivery_place.latitude), longitude: parseFloat(delivery.delivery_place.longitude), ...this.props.map_delta };

                this.updateState({

                    delivery: delivery,
                    pickup_place: pickup_place,
                    delivery_place: delivery_place,
                    directions: true,
                    start: true,
                    trip_title: `Delivery of ${delivery.item}`,
                    recipient: `${delivery.recipient_name} ${delivery.recipient_phone}`,
                    item: delivery.item.length < 20 ? delivery.item : delivery.item.substring(0, 17) + '...'
                });

                if (delivery.status == 'active') this.updateState({ pickup_status: false, start: false, on_trip: true });
                if (delivery.status == 'completed') this.updateState({ pickup_status: false, start: false, on_trip: false });

            }

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    async start() {

        try {

            this.updateState({ pickup_status: false, start: false, on_trip: true });

            let response = await API.get(`driver/delivery-start/${this.props.delivery.id}`, this.props.api_token);

            if (response.status == false)
                Local.toast(response.data.toString());

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    closeModal = () => {

        this.updateState({ pickup_status: false, arrived_status: false });
    }

    async arrived() {

        try {

            this.updateState({ arrived_status: false, start: true, on_trip: false });

            let response = await API.get(`driver/delivery-complete/${this.props.delivery.id}`, this.props.api_token);

            Local.toast(response.data.toString());
            Alert.alert('', 'Delivery has been completed.');

            if (response.status == true) {

                this.updateState(null);
                this.props.navigation.navigate('DriverDeliveries');
            }

        } catch (ex) {

            Local.toast(ex.message);

        }
    }

    render() {

        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Head navigation={this.props.navigation} title={this.props.trip_title} back={true} />

                <View style={styles.container}>

                    <MapView style={styles.map} region={this.props.position} showsMyLocationButton={true} ref={ref => { this.map = ref }} customMapStyle={Local.mapCustomStyle()}>

                        {this.props.pickup_place
                            ? <Marker coordinate={this.props.pickup_place} description={this.props.delivery.pickup_place.address} pinColor={Colors.primary} />
                            : null
                        }

                        {this.props.delivery_place
                            ? <Marker coordinate={this.props.delivery_place} description={this.props.delivery.delivery_place.address} pinColor={Colors.red} />
                            : null
                        }

                        {this.props.directions
                            ? <MapViewDirections origin={this.props.pickup_place} destination={this.props.delivery_place} apikey={GOOGLE_MAPS_APIKEY} strokeWidth={4} strokeColor={Colors.primary} style={{ opacity: 0 }} />
                            : null
                        }


                    </MapView>

                </View>

                <Footer style={styles.footer}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>

                        {this.props.vehicle_image
                            ? <Thumbnail source={{ uri: this.props.vehicle_image }} />
                            : <Thumbnail source={require('../../img/logo.png')} />
                        }

                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>

                            {this.props.item
                                ? <Text style={styles.item}>{this.props.item}</Text>
                                : <Animatable.Text animation='flash' easing='ease-out' iterationCount='infinite' style={styles.item}>Loading...</Animatable.Text>
                            }

                            <Text style={styles.recipient}>{this.props.recipient}</Text>
                        </View>
                    </View>

                    {this.props.start
                        ? <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <Button style={{ borderRadius: 4, backgroundColor: 'transparent', padding: 5 }} onPress={() => this.updateState({ pickup_status: true })}>
                                <Text style={{ color: Colors.primary, fontWeight: 'bold' }}>START&nbsp;</Text>
                                <Icon name='ios-arrow-forward' style={{ color: Colors.primary }} />
                                <Icon name='ios-arrow-forward' style={{ color: Colors.primary }} />
                                <Icon name='ios-arrow-forward' style={{ color: Colors.primary }} />
                            </Button>
                        </View>
                        : null}

                    {this.props.on_trip
                        ? <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>

                            <Button style={{ backgroundColor: 'transparent', padding: 5 }} onPress={() => Linking.openURL(`tel:${this.props.delivery.recipient_phone}`)}>
                                <Icon name='ios-call' style={{ color: Colors.primary }} />
                            </Button>

                            <View style={styles.pipe}></View>

                            <Button style={{ backgroundColor: 'transparent', padding: 10 }} onPress={() => this.updateState({ arrived_status: true })}>
                                <Text style={{ color: Colors.primary, fontWeight: 'bold' }}>ARRIVED</Text>
                            </Button>

                        </View>
                        : null}

                    <ConfirmDialog title='Confirm Pickup' message='Confirm that you have arrived at the Pickup Location and also Picked up the Parcel.' visible={this.props.pickup_place_status} onTouchOutside={() => this.updateState({ pickup_status: false })} positiveButton={{ title: 'Confirm', onPress: () => this.start() }} negativeButton={{ title: 'Close', onPress: () => this.closeModal() }} />
                    <ConfirmDialog title='Confirm Arrival' message='Confirm that you have arrived at the Destination and also Delivered the Parcel.' visible={this.props.arrived_status} onTouchOutside={() => this.updateState({ arrived_status: false })} positiveButton={{ title: 'Confirm', onPress: () => this.arrived() }} negativeButton={{ title: 'Close', onPress: () => this.closeModal() }} />

                </Footer>
            </View>
        );
    }
}

const styles = Styles.DriverTrip;

const mapStateToProps = state => ({ ...state.driverTrip, ...state.driverHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverTrip);
