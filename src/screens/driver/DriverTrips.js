import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Text, Icon, ListItem, Body, Right } from 'native-base';
import Head from '../../components/Head';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Moment from 'moment';
import Styles from '../../utils/Styles';
import { DRIVER_DELIVERIES } from '../../redux/types';

class DriverTrips extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: DRIVER_DELIVERIES, payload: state });
    }

    async componentWillMount() {

        try {

            let response = await API.get('driver/deliveries', this.props.api_token);

            if (response.status == true) {

                if (response.data.completed)
                    this.updateState({ completed: response.data.completed });
            }

        } catch (ex) {

            Local.toast('Could not get your deliveries at this time.');
        }

    }

    renderDelivery = data => {

        return (

            <TouchableOpacity key={data.id}>
                <ListItem style={{ margin: 0 }} onPress={() => this.tripDetail(data)} nopadding>
                    <Body style={{ margin: 0, padding: 0 }}>
                        <Text>{data.pickup_place.address}</Text>
                        <Text note style={{ fontSize: 12 }}>Delivered {data.item} for {`${data.user.first_name} ${data.user.last_name}`}</Text>
                        <Text note style={{ fontSize: 8 }}>{Moment(data.created_at).format('MMMM Do YYYY, h:mm a')} <Text style={{ color: Colors.green, fontSize: 7 }}>completed</Text></Text>
                    </Body>
                    <Right>
                        <Icon name='ios-arrow-forward' />
                    </Right>
                </ListItem>
            </TouchableOpacity>
        )
    }

    tripDetail = data => {

        this.props.navigation.navigate('DriverTripDetail', { delivery: data });
    }

    render() {

        let completed = this.props.completed.map((row, index) => this.renderDelivery(row));

        return (

            <View style={styles.container}>

                <Head navigation={this.props.navigation} title='Trips' back={true} />

                {this.props.completed.length < 1
                    ? <ImageBackground source={require('../../img/deliveries.png')} style={styles.background}>
                        <Text style={styles.info}>All your completed trips will show up here. You would be able to see detailed information such as how much you earn on each of them and lots more.</Text>
                    </ImageBackground>
                    : <View style={{ padding: 10, }}>
                        <ScrollView style={{ marginBottom: 20 }}>
                            {completed}
                        </ScrollView>
                    </View>
                }

                <View style={{ height: 30 }}></View>

            </View>
        )
    }
}

const styles = Styles.DriverTrips;

const mapStateToProps = state => ({ ...state.driverDeliveries, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(DriverTrips);

