import React, { Component } from 'react';
import { View } from 'react-native';
import { Text, Tabs, Tab } from 'native-base';
import { Image, Button } from '@shoutem/ui';
import Head from '../../components/Head';
import Colors from '../../utils/Colors';

export default class DriverWallet extends Component {


  render() {

    return (

      <View style={{ backgroundColor: '#f3f4f6', width: '100%', height: '100%' }}>

        <Head navigation={this.props.navigation} title='Wallet' back={true} />

        <View style={{ padding: 30, paddingTop: 30, backgroundColor: Colors.primary, alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>
            <Image styleName='medium-avatar' style={{ width: 80, height: 80 }} source={{ uri: 'https://firebasestorage.googleapis.com/v0/b/velocity-8da3a.appspot.com/o/nigeria-naira-currency-symbol.png?alt=media&token=6a58732e-3e24-499f-b5a5-d688b2196e83' }} />

            <Text style={{ color: Colors.white, fontSize: 90, marginTop: -20, marginLeft: 10 }}>0.00</Text>
          </View>

          <Text style={{ color: Colors.white, fontSize: 18 }}>Your balance</Text>
          <Button style={{ padding: 10, marginTop: 40, width: '50%' }}>
            <Text style={{ color: Colors.primary }}>Fund Wallet</Text>
          </Button>
        </View>

        <Tabs>

          <Tab heading='WITHDRAWALS' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: '#000000' }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'normal' }}></Tab>
          <Tab heading='DEPOSIT' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: '#000000' }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'normal' }}></Tab>
        </Tabs>

      </View>
    )
  }
}
