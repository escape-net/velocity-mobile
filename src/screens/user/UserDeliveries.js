import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, ImageBackground } from 'react-native';
import { Tabs, Tab, Text } from 'native-base';
import Head from '../../components/Head';
import Delivery from '../../components/Delivery';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { USER_DELIVERIES } from '../../redux/types';

class UserDeliveries extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_DELIVERIES, payload: state });
    }

    async componentDidMount() {

        try {

            let response = await API.get('user/deliveries', this.props.api_token);

            if (response.status == true) {

                let { data } = response;

                if (data.completed) this.updateState({ completed: data.completed });
                if (data.active) this.updateState({ active: data.active });

            } else {

                Local.toast(response.data.toString());
            }

        } catch (ex) {

            Local.toast('Could not get your deliveries at this time.');
        }

    }

    renderDelivery = data => {

        return (<Delivery delivery={data} key={data.id} onPress={data.status == 'completed' ? this.detail : this.trip} />);
    }

    trip = data => {

        this.props.navigation.navigate('UserTrip', { id: data.id });
    }

    detail = data => {

        this.props.navigation.navigate('UserTripDetail', { delivery: data });
    }

    render() {

        let completed = this.props.completed.map((row, index) => this.renderDelivery(row));
        let active = this.props.active.map((row, index) => this.renderDelivery(row));

        return (

            <View style={styles.container}>

                <Head navigation={this.props.navigation} title='Deliveries' back={true} />

                <Tabs>

                    <Tab heading='Past' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: Colors.black }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'bold' }}>

                        {this.props.completed.length < 1
                            ? <ImageBackground source={require('../../img/deliveries.png')} style={styles.background}>
                                <Text style={styles.info}>All your previously completed deliveries will show up here.</Text>
                            </ImageBackground>
                            : <View style={{ padding: 10 }}>
                                <ScrollView>{completed}</ScrollView>
                            </View>
                        }

                    </Tab>

                    <Tab heading='Upcoming' tabStyle={{ backgroundColor: Colors.white }} textStyle={{ color: Colors.black }} activeTabStyle={{ backgroundColor: Colors.white }} activeTextStyle={{ color: Colors.primary, fontWeight: 'bold' }}>

                        {this.props.active.length < 1
                            ? <ImageBackground source={require('../../img/deliveries.png')} style={styles.background}>
                                <Text style={styles.info}>Your Upcoming and Active Deliveries will be avaliable here for you to track them.</Text>
                            </ImageBackground>
                            : <View style={{ padding: 10 }}>
                                <ScrollView>{active}</ScrollView>
                            </View>
                        }

                    </Tab>

                </Tabs>

            </View>
        )
    }
}

const styles = Styles.UserDeliveries;

const mapStateToProps = state => ({ ...state.userDeliveries, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserDeliveries);

