import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Alert, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input, ListItem, Body, Text, Icon, Separator } from 'native-base';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import Head from '../../components/Head';
import Addresses from '../../components/Addresses';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { USER_DELIVERY_DETAILS, USER_HOME, USER_TRIP } from '../../redux/types';

class UserDeliveryDetails extends Component {

    async componentDidMount() {
         //removed as this is now a duplicate
        // try {

            // let payload = {

                // vehicle_type_id: this.props.vehicle_type_id,
                // pickup_latitude: this.props.pickup.latitude,
                // pickup_longitude: this.props.pickup.longitude,
                // delivery_latitude: this.props.delivery.latitude,
                // delivery_longitude: this.props.delivery.longitude,
            // };

            // let response = await API.post('user/delivery/estimate', payload, this.props.api_token);
            // if (response.status == true)
                // this.updateState({ estimate_amount: response.data });

        // } catch (ex) {

            // Local.toast(ex.message);
        // }
    }

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_DELIVERY_DETAILS, payload: state });
    }

    async next() {

        try {

            this.updateState({ loading: true });

            let response = await API.post('user/delivery/new', {

                item: this.props.item,
                vehicle_type_id: this.props.vehicle_type_id,
                pickup: this.props.pickup,
                delivery: this.props.delivery,
                payment: this.props.payment,
                recipient_name: this.props.recipient_name,
                recipient_phone: this.props.recipient_phone,
                instructions: this.props.instructions

            }, this.props.api_token);

            if (response.status == true) {

                this.props.dispatch({ type: USER_HOME, payload: null });
                this.props.dispatch({ type: USER_TRIP, payload: null });
                this.props.dispatch({ type: USER_TRIP, payload: { delivery: response.data } });
                this.updateState({ loading: false });
                this.props.navigation.navigate('UserTrip');

            } else {

                Alert.alert('Oppzz', response.data.toString());
                this.updateState({ loading: false });
            }

        } catch (ex) {

            console.log(ex);
            Local.toast(ex.message);
        }

    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head navigation={this.props.navigation} title='Delivery Details' back={true} />

                <Addresses pickup={this.props.pickup_address} delivery={this.props.delivery_address} />

                <ListItem style={{ margin: 0, borderBottomWidth: 0 }}>
                    <Body>
                        {this.props.estimate
                            ? <View>
                                <Text note>Estimated Price</Text>
                                <Text>{this.props.estimate_amount}</Text>
                            </View>
                            : <TouchableOpacity onPress={() => this.updateState({ estimate: true })}>
                                <Text style={{ color: Colors.red, textAlign: 'right' }}>Show Estimate</Text>
                            </TouchableOpacity>
                        }

                    </Body>
                </ListItem>


                <View style={{ marginTop: 20 }}>
                    <Separator bordered>
                        <Text style={{ fontSize: 15 }}>Item Details</Text>
                    </Separator>
                </View>


                <View style={{ margin: 0, padding: 20 }}>

                    <Item floatingLabel style={{ marginBottom: 10 }}>
                        <Label style={{ marginTop: 5 }}>Item</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ item: text })} value={this.props.item} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 10 }}>
                        <Label style={{ marginTop: 5 }}>Reciever's Name</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ recipient_name: text })} value={this.props.recipient_name} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 10 }}>
                        <Label style={{ marginTop: 5 }}>Reciever's Phone</Label>
                        <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ recipient_phone: text })} value={this.props.recipient_phone} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Delivery Instructions</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ instructions: text })} value={this.props.instructions} />
                    </Item>

                    <VButton onPress={() => this.next()} text='NEXT' loading={this.props.loading} background={Colors.primary} color={Colors.white}></VButton>

                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.UserDeliveryDetails;

const mapStateToProps = state => ({ ...state.userDeliveryDetails, ...state.userHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserDeliveryDetails);
