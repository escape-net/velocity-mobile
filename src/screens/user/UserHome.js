import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Text, Icon as NIcon } from 'native-base';
import { Button, Text as SText, View as SView } from '@shoutem/ui';
import Head from '../../components/Head';
import { StackActions, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import MapView, { Marker, Callout } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import * as Animatable from 'react-native-animatable';
import VButton from '../../components/VButton';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { USER_HOME, AUTH, USER_DELIVERY_DETAILS } from '../../redux/types';

const GOOGLE_MAPS_APIKEY = Local.googleMapsKey();

class UserHome extends Component {

    constructor(props) {

        super(props);

        this.state = { loading: false };
    }

    async componentDidMount() {

        try {

            let network = await NetInfo.getConnectionInfo();

            if (network.type == 'none') {

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Offline' })] }));
                return;
            }
			
            if (this.props.pickup == null) { 
                
                let location = await Local.getCurrentPosition();

                if (location.error) {

                    Local.toast(location.error);

                } else {
					
                    let { latitude, longitude, accuracy } = location;
					let { width, height } = Dimensions.get('window');
					let map_delta = Local.googleMapsDelta(width, height);
                    this.props.dispatch({ type: AUTH, payload: { map_delta } });
					
                    let place = await Local.getGooglePlace(latitude, longitude);

                    if (place.latitude && place.longitude && this.props.map_delta) {
					    let root = { latitude: latitude, longitude: longitude, ...this.props.map_delta };
                        let pickup = { latitude: place.latitude, longitude: place.longitude, address: place.address, place_id: place.place_id };
                        let pickup_address = place.address;

                        this.props.dispatch({ type: USER_HOME, payload: { root: root, pickup: pickup, pickup_address: pickup_address } });

                        if (this.pickupMarker)
                            setTimeout(() => this.pickupMarker.showCallout());
                    }
                }

                if (this.props.root && this.map)
                    setTimeout(() => this.map.fitToElements(true));
            }

            let response = await API.get('vehicle-types', this.props.api_token);
            if (response.status == true)
                this.props.dispatch({ type: USER_HOME, payload: { vehicle_types: response.data } });

            let places = await API.get('user/places', this.props.api_token);
            if (places.status == true)
                this.props.dispatch({ type: USER_HOME, payload: { places: places.data } });

			
			
            if (this.props.pickup && this.props.delivery)
                this.props.dispatch({ type: USER_HOME, payload: { directions: true } });

            this.props.navigation.addListener('willFocus', () => {

                if (this.props.pickup && this.pickupMarker){
                    this.pickupMarker.showCallout();
					 
				}

                if (this.props.delivery && this.deliveryMarker)
                    this.deliveryMarker.showCallout();

                if (this.props.pickup && this.props.delivery)
                    this.props.dispatch({ type: USER_HOME, payload: { directions: true } });

                if (this.map) setTimeout(() => this.map.fitToElements(true));
            });

        } catch (ex) {

            Local.toast(ex.message);
        }
    }
	
	

    next = async (id, name) => {

        this.props.dispatch({ type: USER_HOME, payload: { vehicle_type_id: id, delivery_mode: `Delivery By ${name}` } })

        if (this.map) setTimeout(() => this.map.fitToElements(true));
    }
	
	
	//calculate estimate and navigate to details page
	async proceedToDetails(){
		try {
            this.setState({loading : true});
            let payload = {

                vehicle_type_id: this.props.vehicle_type_id,
                pickup_latitude: this.props.pickup.latitude,
                pickup_longitude: this.props.pickup.longitude,
                delivery_latitude: this.props.delivery.latitude,
                delivery_longitude: this.props.delivery.longitude,
            };

            let response = await API.post('user/delivery/estimate', payload, this.props.api_token);
            if (response.status == true){
                this.updateState({ estimate_amount: response.data });
				this.setState({loading : false}); 
			this.props.navigation.navigate('UserDeliveryDetails');
			}

        } catch (ex) {

            Local.toast(ex.message);
        }
	}
	
	
    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_DELIVERY_DETAILS, payload: state });
    } 
	
	async cancelDelivery(){ 
		this.props.dispatch({ type: USER_HOME, payload: { directions: false, pickup : null, pickup_address : null, delivery: null, delivery_address : null }});
		
		let location = await Local.getCurrentPosition();

                if (location.error) {

                    Local.toast(location.error);

                } else {
					
                    let { latitude, longitude, accuracy } = location;
					let { width, height } = Dimensions.get('window');
					let map_delta = Local.googleMapsDelta(width, height);
                    this.props.dispatch({ type: AUTH, payload: { map_delta } });
					
                    let place = await Local.getGooglePlace(latitude, longitude);

                    if (place.latitude && place.longitude && this.props.map_delta) {
					    let root = { latitude: latitude, longitude: longitude, ...this.props.map_delta };
                        let pickup = { latitude: place.latitude, longitude: place.longitude, address: place.address, place_id: place.place_id }
                        let pickup_address = place.address;

                        this.props.dispatch({ type: USER_HOME, payload: { root: root, pickup: pickup, pickup_address: pickup_address } });

                        if (this.pickupMarker)
                            setTimeout(() => this.pickupMarker.showCallout());
                    }
                }

                if (this.props.root && this.map)
                    setTimeout(() => this.map.fitToElements(true));
            
	}
	
	

    render() {

        return (

            <View>

                <Head navigation={this.props.navigation} right={true} rightPress={() => { }} rightIcon='time' />

                <View style={styles.container}>

                    <MapView style={styles.map} region={this.props.root}  maxZoomLevel={15} showsMyLocationButton={true} ref={ref => { this.map = ref }} customMapStyle={Local.mapCustomStyle()}>

                        {this.props.pickup
                            ? <Marker draggable coordinate={this.props.pickup} ref={marker => this.pickupMarker = marker} onCalloutPress={() => this.props.navigation.navigate('UserSearchPlace', { type: 'pickup' })} pinColor={Colors.primary}>
                                <Callout tooltip={true}>
                                    <View style={styles.callout}>
                                        <Text style={{ color: Colors.primary, fontSize: 9, textAlign: 'center' }}>{this.props.pickup_address}</Text>
                                    </View>
                                </Callout>
                            </Marker>
                            : null
                        }

                        {this.props.delivery
                            ? <Marker draggable coordinate={this.props.delivery} ref={marker => this.deliveryMarker = marker} onCalloutPress={() => this.props.navigation.navigate('UserSearchPlace', { type: 'delivery' })} pinColor={Colors.red}>
                                <Callout tooltip={true}>
                                    <View style={styles.callout}>
                                        <Text style={{ color: Colors.red, fontSize: 9, textAlign: 'center' }}>{this.props.delivery_address}</Text>
                                    </View>
                                </Callout>
                            </Marker>
                            : null
                        }

                        {this.props.directions
                            ? <MapViewDirections origin={this.props.pickup} destination={this.props.delivery} apikey={GOOGLE_MAPS_APIKEY} strokeWidth={4} strokeColor={Colors.primary} style={{ opacity: 0 }} />
                            : null
                        }

                    </MapView>

                    <View style={{ backgroundColor: 'transparent', margin: 20, bottom: '80%' }}>

                        <View style={styles.pickup_box}>

                            <View style={{ width: '5%' }}>
                                <NIcon name='md-locate' style={{ color: Colors.green, fontSize: 15, marginTop: 2 }} />
                            </View>

                            <View style={{ width: '95%', paddingLeft: 5, }}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('UserSearchPlace', { type: 'pickup' }) }}>

                                    {this.props.pickup_address
                                        ? <Text numberOfLines={1} style={{ fontSize: 14 }}>{this.props.pickup_address}</Text>
                                        : <Text style={{ fontSize: 14, color: Colors.grey }}>Select Pickup Address</Text>
                                    }

                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={styles.delivery_box}>

                            <View style={{ width: '5%' }}>
                                <NIcon name='md-pin' style={{ color: 'red', fontSize: 15, marginTop: 2 }} />
                            </View>

                            <View style={{ width: '95%', paddingLeft: 5, }}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('UserSearchPlace', { type: 'delivery' }) }}>

                                    {this.props.delivery_address
                                        ? <Text numberOfLines={1} style={{ fontSize: 14 }}>{this.props.delivery_address}</Text>
                                        : <Text style={{ fontSize: 14, color: Colors.grey }}>Select Drop-Off Address</Text>
                                    }
                                </TouchableOpacity>
                            </View>

                        </View>

                    </View>

                </View>

                {this.props.directions
                    ? <Animatable.View animation='bounceInUp' easing='ease-out' iterationCount={1} style={styles.footer}>

                        <View style={styles.footerInner}>

                            <Text style={{ textAlign: 'center', color: Colors.grey, padding: 7 }}>{this.props.delivery_mode}</Text>

                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                                {this.props.vehicle_types.map(row => (

                                    <View key={row.id.toString()} style={[styles.vehicle, { backgroundColor: this.props.vehicle_type_id == row.id ? Colors.primary : Colors.greyLight }]}>
                                        <TouchableOpacity onPress={() => this.next(row.id, row.name)} style={{ borderRadius: 20 }}>
                                            <Icon name={`${row.mobile_icon}`} size={25} style={[styles.vehicleIcon, { color: this.props.vehicle_type_id == row.id ? Colors.white : Colors.grey }]} />
                                        </TouchableOpacity>
                                    </View>
                                ))}

                            </ScrollView>

                            {this.props.payment
                                ? null
                                : <Text style={{ textAlign: 'center', color: Colors.grey, padding: 7 }}>Select Payment Method</Text>
                            }


                            {this.props.payment
                                ? <SView styleName='vertical'>
								<VButton onPress={() =>  this.props.dispatch({ type: USER_HOME, payload: { payment: null } })} text='Change Payment Method'  background={Colors.primary} color={Colors.white}></VButton> 
								<VButton onPress={() => this.proceedToDetails() } text='Next' loading={this.state.loading} background={Colors.primary} color={Colors.white}></VButton>
                                </SView> 
								: 
								<SView styleName='vertical'>
								<SView styleName='horizontal'>
                                    <Button styleName='confirmation' style={{ backgroundColor: Colors.green }} onPress={() => this.props.dispatch({ type: USER_HOME, payload: { payment: 'card' } })}>
                                        <Icon name='credit-card' style={{ color: Colors.white }} size={20} />
                                        <SText style={{ color: Colors.white, fontSize: 16 }}>&nbsp;CARD</SText>
                                    </Button>
                                    <Button styleName='confirmation' style={{ backgroundColor: Colors.green }} onPress={() => this.props.dispatch({ type: USER_HOME, payload: { payment: 'cash' } })}>
                                        <Icon name='attachment' style={{ color: Colors.white }} size={20} />
                                        <SText style={{ color: Colors.white, fontSize: 16 }}>&nbsp;CASH</SText>
                                    </Button>
										
                                </SView>
								<TouchableOpacity onPress={() => this.cancelDelivery()}> 
                                            <Text style={[styles.cancel, { color: Colors.red, textAlign: 'center' }]}><Icon name='close' style={{ color: Colors.red, fontSize: 12 }} />&nbsp;CANCEL DELIVERY</Text>
                                        </TouchableOpacity>
							   </SView>
                            }

                        </View>

                    </Animatable.View>

                    : null}
            </View>
        );
    }
}

const styles = Styles.UserHome;

const mapStateToProps = state => ({ ...state.userHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserHome);