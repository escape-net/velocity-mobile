import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity } from 'react-native';
import { Text, ListItem, Thumbnail, Left, Body } from 'native-base';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Head from '../../components/Head';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';

class UserPayments extends Component {


    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={{ backgroundColor: Colors.white, height: '100%', padding: 0 }} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head navigation={this.props.navigation} title='Payments' back={true} />

                <View style={{ backgroundColor: Colors.white }}>

                    <View style={{ marginBottom: 15, padding: 10, width: '100%', backgroundColor: Colors.greyLight }}>
                        <Text style={{ fontSize: 20, color: Colors.grey }}>Payment Methods</Text>
                    </View>

                    <ListItem icon style={{ marginBottom: 15 }}>
                        <Left>
                            <Icon active name='add' style={{ color: Colors.black, fontSize: 20 }} />
                        </Left>
                        <Body style={{ borderBottomWidth: 0 }}>
                            <TouchableOpacity onPress={() => { }}>
                                <Text style={{ color: Colors.black, fontSize: 20 }}>Add Payment Card</Text>
                            </TouchableOpacity>
                        </Body>
                    </ListItem>

                    <View style={styles.hr}></View>

                    <ListItem avatar style={{ marginTop: 15, marginBottom: 15 }}>
                        <Left>
                            <Thumbnail source={require('../../img/avatar.png')} />
                        </Left>
                        <Body style={{ borderBottomWidth: 0 }}>
                            <Text style={{ color: Colors.black, fontSize: 20 }}>**** 9738</Text>
                            <Text note>14/2019</Text>
                        </Body>
                    </ListItem>

                    <View style={styles.hr}></View>

                    <ListItem icon style={{ marginTop: 15, marginBottom: 15 }}>
                        <Left>
                            <Icon active name='attachment' style={{ color: Colors.black, fontSize: 20 }} />
                        </Left>
                        <Body style={{ borderBottomWidth: 0 }}>
                            <Text style={{ color: Colors.black, fontSize: 20 }}>Cash</Text>
                        </Body>
                    </ListItem>

                    <View style={{ marginBottom: 15, padding: 10, width: '100%', backgroundColor: Colors.greyLight }}>
                        <Text style={{ fontSize: 20, color: Colors.grey }}>Promotions</Text>
                    </View>

                    <ListItem icon style={{ marginBottom: 15 }}>
                        <Left>
                            <Icon active name='redeem' style={{ color: Colors.black, fontSize: 20 }} />
                        </Left>
                        <Body style={{ borderBottomWidth: 0 }}>
                            <TouchableOpacity onPress={() => { }}>
                                <Text style={{ color: Colors.black, fontSize: 20 }}>Apply Promo code</Text>
                            </TouchableOpacity>
                        </Body>
                    </ListItem>

                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.UserPayments;

const mapStateToProps = state => ({ ...state.userPayments, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserPayments);
