import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Text, Grid, Col, Row, Thumbnail, Container, Item, Label, Input } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import VButton from '../../components/VButton';
import PhotoUpload from 'react-native-photo-upload';
import { SkypeIndicator } from 'react-native-indicators';
import Head from '../../components/Head';
import Local from '../../utils/Local';
import API from '../../utils/API';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { USER_PROFILE, AUTH } from '../../redux/types';

class UserProfile extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_PROFILE, payload: state });
    }

    async componentDidMount() {

        try {

            let { user } = this.props;

            this.updateState({

                first_name: user.first_name,
                last_name: user.last_name,
                phone: user.phone,
                email: user.email,
                photo: user.photo
            });

            if (user.vehicle) {

                if (user.vehicle.plate_number) this.updateState({ plate_number: user.vehicle.plate_number });

                if (user.vehicle.make && user.vehicle.model && user.vehicle.year && user.vehicle.color) {

                    let { make, model, year, color } = user.vehicle;
                    this.updateState({ vehicle: `${make}/${model}/${year}/${color}` });
                }

            }

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    async edit() {

        try {

            this.updateState({ profile_loading: true });

            let response = await API.post('driver/edit-profile', {

                first_name: this.props.first_name,
                last_name: this.props.last_name,
                email: this.props.email,
                phone: this.props.phone

            }, this.props.api_token);

            if (response.status == true) {

                this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                this.updateState({ edit: false, profile_loading: false });

            } else {

                Local.toast(response.data.toString());
                this.updateState({ profile_loading: false });

            }

        } catch (ex) {

            Local.toast(ex.message);
            this.updateState({ profile_loading: false });

        }
    }

    async upload(image) {

        try {

            let response = await API.post('driver/upload/photo', { image: image }, this.props.api_token);

            if (response) {

                if (response.status == true) {

                    this.props.dispatch({ type: AUTH, payload: { user: response.data } });
                    this.updateState({ photo: response.data.photo });

                } else {

                    Local.toast(response.data.toString());

                }

                setTimeout(() => this.updateState({ profile_loading: false }), 1000);

            }

        } catch (ex) {

            this.updateState({ profile_loading: false });
        }


    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head navigation={this.props.navigation} title='Profile' back={true} right={true} rightPress={() => this.updateState({ edit: true })} rightIcon='md-create' />

                <View style={styles.header}>

                    <PhotoUpload onPhotoSelect={image => this.upload(image)} onResponse={() => this.updateState({ profile_loading: true })}>
                        {this.props.profile_loading
                            ? <SkypeIndicator color={Colors.white} />
                            : this.props.photo
                                ? <Thumbnail large source={{ uri: this.props.photo }} style={styles.thumbnail} />
                                : <Thumbnail large source={require('../../img/avatar.png')} style={styles.thumbnail} />}

                    </PhotoUpload>

                </View>

                {this.props.edit
                    ? <Animatable.View animation='fadeIn' easing='ease-out' iterationCount={1} style={{ margin: 0, padding: 10 }}>

                        <Item floatingLabel style={{ marginBottom: 30, marginTop: 30 }}>
                            <Label style={{ marginTop: 5 }}>Phone Number</Label>
                            <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.updateState({ phone: text })} value={this.props.phone} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 30 }}>
                            <Label style={{ marginTop: 5 }}>First Name</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ first_name: text })} value={this.props.first_name} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 30 }}>
                            <Label style={{ marginTop: 5 }}>Last Name</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ last_name: text })} value={this.props.last_name} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 30 }}>
                            <Label style={{ marginTop: 5 }}>Email Address</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ email: text })} value={this.props.email} />
                        </Item>


                        <VButton onPress={this.edit.bind(this)} text='SAVE' profile_loading={this.props.profile_loading} background={Colors.primary} color={Colors.white}></VButton>
                    </Animatable.View>
                    : <Container>
                        <Grid style={styles.top}>
                            <Row>
                                <Col>
                                    <Text style={styles.h1}>First Name</Text>
                                    <Text>{this.props.first_name}</Text>
                                </Col>
                                <Col>
                                    <Text style={styles.h1}>Last Name</Text>
                                    <Text>{this.props.last_name}</Text>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Text style={styles.h1}>Phone</Text>
                                    <Text>{this.props.phone}</Text>
                                </Col>
                                <Col>
                                    <Text style={styles.h1}>Email</Text>
                                    <Text>{this.props.email}</Text>
                                </Col>
                            </Row>
                        </Grid>

                        <Grid style={styles.top}>
                        </Grid>

                        <View style={styles.hr} />

                    </Container>}

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.UserProfile;

const mapStateToProps = state => ({ ...state.userProfile, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);