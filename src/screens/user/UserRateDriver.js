import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Item, Label, Input, Text, Icon } from 'native-base';
import StarRating from 'react-native-star-rating';
import VButton from '../../components/VButton';
import Colors from '../../utils/Colors';
import Head from '../../components/Head';
import Addresses from '../../components/Addresses';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Styles from '../../utils/Styles';
import { USER_RATE_DRIVER, USER_HOME, USER_DELIVERY_DETAILS, USER_TRIP } from '../../redux/types';

class UserRateDriver extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_RATE_DRIVER, payload: state });
    }

    submit = async () => {

        this.updateState({ rate_loading: true });

        setTimeout(() => {

            Alert.alert('Thank You', 'Thank you for your Review.');

            this.props.dispatch({ type: USER_HOME, payload: null });
            this.props.dispatch({ type: USER_DELIVERY_DETAILS, payload: null });
            this.props.dispatch({ type: USER_TRIP, payload: null });
            this.props.dispatch({ type: USER_RATE_DRIVER, payload: null });

            this.props.navigation.navigate('UserHome');

        }, 1000);
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head navigation={this.props.navigation} title='Rate your Trip' back={true} />

                <Addresses pickup='14, Farayibi Street, Bariga Lagos' delivery='Gbagada Industrial Estate lagos' />

                <Icon name='checkmark-circle' style={styles.check} />
                <Text style={{ textAlign: 'center', color: Colors.primary }}>Delivery Successful!</Text>

                <View style={{ margin: 10, padding: 20, borderWidth: 1, borderColor: Colors.greyLight }}>

                    <Text style={{ textAlign: 'center', color: Colors.grey }}>Rate this Delivery</Text>

                    <View style={{ padding: 30, margin: 20 }}>
                        <StarRating maxStars={5} rating={this.props.rate} selectedStar={rating => this.updateState({ rate: rating })} fullStarColor={Colors.primary} halfStarEnabled={true} />
                    </View>

                    <Item floatingLabel style={{ marginBottom: 15 }}>
                        <Label style={{ marginTop: 5 }}>Comments</Label>
                        <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.updateState({ comment: text })} value={this.props.comment} />
                    </Item>

                    <VButton onPress={() => this.submit()} text='SUBMIT' loading={this.props.rate_loading} background={Colors.primary} color={Colors.white}></VButton>

                </View>

            </KeyboardAwareScrollView>
        )
    }
}

const styles = Styles.UserRateDriver;

const mapStateToProps = state => ({ ...state.userRateDriver, ...state.userTrip, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserRateDriver);

