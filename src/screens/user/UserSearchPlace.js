import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Dimensions } from 'react-native';
import Head from '../../components/Head';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { USER_HOME, AUTH } from '../../redux/types';

class UserSearchPlace extends Component {

    componentDidMount() {

        this.GooglePlacesRef.setAddressText('');

        this.props.navigation.addListener('willFocus', () => {

            this.GooglePlacesRef.setAddressText('');
        });
    }

    onPress = (details, data) => {

        try {

		if (!this.props.map_delta) {

                let { width, height } = Dimensions.get('window');
                let map_delta = Local.googleMapsDelta(width, height); 

                this.props.dispatch({ type: AUTH, payload: { map_delta } });
		} 
		
		  

            let type = this.props.navigation.getParam('type');
            type = type == 'pickup' ? 'pickup' : 'delivery';

            let { lat, lng } = data.geometry.location;
            let coords = { place_id: data.place_id, latitude: parseFloat(lat), longitude: parseFloat(lng), address: data.formatted_address, ...this.props.map_delta };
            
			if (type == 'pickup') {

                this.props.dispatch({ type: USER_HOME, payload: {  pickup_address: data.formatted_address, pickup: coords } });
            }

            if (type == 'delivery') {

                this.props.dispatch({ type: USER_HOME, payload: { delivery_address: data.formatted_address, delivery: coords } });
            }

            if (this.props.pickup && this.props.delivery)
                this.props.dispatch({ type: USER_HOME, payload: { directions: true } });

            return this.props.navigation.goBack();

        } catch (ex) {

            Local.toast(ex);
        }

    }
	

    render() {

        let places = this.props.places.filter(row => row.type == this.props.navigation.getParam('type'));
			
        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Head navigation={this.props.navigation} title='Set Location' back={true} />

                <GooglePlacesAutocomplete
                    ref={(instance) => this.GooglePlacesRef = instance}
                    placeholder='Search'
                    minLength={2}
                    autoFocus={true}
                    returnKeyType={'search'}
                    listViewDisplayed='auto'
                    fetchDetails={true}
                    onPress={(data, details = null) => this.onPress(data, details)}
                    getDefaultValue={() => ''}
                    query={{
						key: Local.googleMapsKey(),
    					language: 'en',
						region: 'ng',
					    components:'country:ng', //restrict location to 150km around user and limit to nigeria
						 radius: '150000', 
						 strictbounds: true
					}}
                    styles={{ textInputContainer: { width: '100%' }, description: { fontWeight: 'bold' }, predefinedPlacesDescription: { color: Colors.primary } }}
                    currentLocation={true}
                    currentLocationLabel='Current location'
                    nearbyPlacesAPI={'GoogleReverseGeocoding'}
                    GooglePlacesSearchQuery={{ rankby: 'distance' }}
                    GooglePlacesDetailsQuery={{ fields: 'formatted_address' }}
                    //filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                    predefinedPlaces={[...places]}
                    debounce={200}

                />

            </View>
        )
    }
}

const mapStateToProps = state => ({ ...state.userHome, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserSearchPlace);
