import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Linking, Dimensions, TouchableOpacity } from 'react-native';
import { Text, Footer, Thumbnail, Icon } from 'native-base';
import MapView, { Marker } from 'react-native-maps';
import * as Animatable from 'react-native-animatable';
import { Button } from '@shoutem/ui';
import { DotIndicator } from 'react-native-indicators';
import Head from '../../components/Head';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import API from '../../utils/API';
import Local from '../../utils/Local';
import Colors from '../../utils/Colors';
import Styles from '../../utils/Styles';
import { USER_TRIP, USER_DELIVERY_DETAILS, AUTH } from '../../redux/types';

class UserTrip extends Component {

    updateState = (state = {}) => {

        this.props.dispatch({ type: USER_TRIP, payload: state });
    }

    async componentDidMount() {

        try {

            this.props.dispatch({ type: USER_DELIVERY_DETAILS, payload: null });

            if (!this.props.map_delta) {

                let { width, height } = Dimensions.get('window');
                let map_delta = Local.googleMapsDelta(width, height);

                this.props.dispatch({ type: AUTH, payload: { map_delta } });
            }

            let { pickup_place, delivery_place } = this.props.delivery;

            pickup_place = {

                latitude: parseFloat(pickup_place.latitude),
                longitude: parseFloat(pickup_place.longitude),
                ...this.props.map_delta
            };

            delivery_place = {

                latitude: parseFloat(delivery_place.latitude),
                longitude: parseFloat(delivery_place.longitude),
                ...this.props.map_delta
            };

            this.updateState({ pickup_place, delivery_place });

            setTimeout(() => this.search(), 1000);
            this.interval = setInterval(() => this.search(), 1000 * 30);

            this.props.navigation.addListener('willFocus', () => {

                if (this.props.search == true) {

                    this.search();
                    this.updateState({ search: false });

                }

                if (this.props.driver) clearInterval(this.interval);

            });

        } catch (ex) {

            Local.toast(ex.message);
        }
    }

    search = async (exclude_driver_id = 0) => {

        console.log('calling Search');

        if (this.props.driver) return clearInterval(this.interval);

        if (!this.props.pickup_place || !this.props.delivery_place) {

            Local.toast('Pickup or Delivery Place not properly set.');
            clearInterval(this.interval);

            setTimeout(() => this.props.navigation.navigate('UserHome'));
        }

        let response = await API.post(`user/assign-driver`, {

            latitude: this.props.pickup_place.latitude,
            longitude: this.props.pickup_place.longitude,
            delivery_id: this.props.delivery.id,
            exclude_driver_id: exclude_driver_id

        }, this.props.api_token);

        console.log('search result', response);

        if (response.status == true) {

            let { data } = response;

            this.updateState({ driver: data, trip_title: `Delivery of ${this.props.delivery.item}`, searching: false });

            if (data.latitude && data.longitude) {

                let driver_location = { latitude: parseFloat(data.latitude), longitude: parseFloat(data.longitude), ...this.props.map_delta };
                this.updateState({ driver_location: driver_location });
            }

            clearInterval(this.interval);

            if (this.map) setTimeout(() => this.map.fitToElements(true));

        } else {

            Local.toast(response.data.toString());
        }
    }

    componentWillUnmount() {

        clearInterval(this.interval);
        this.updateState(null);
    }

    render() {

        return (

            <View style={{ backgroundColor: Colors.white, width: '100%', height: '100%' }}>

                <Head navigation={this.props.navigation} title={this.props.trip_title} back={true} />

                <View style={styles.container}>

                    {this.props.pickup_place
                        ? <MapView style={styles.map} region={this.props.pickup_place} showsMyLocationButton={true} ref={ref => { this.map = ref }} customMapStyle={Local.mapCustomStyle()}>

                            {this.props.driver_location
                                ? <Marker draggable coordinate={this.props.driver_location} ref={marker => this.driverMarker = marker} pinColor={Colors.primary} />
                                : null
                            }

                        </MapView>
                        : null}

                </View>

                <Footer style={styles.footer}>

                    {this.props.searching
                        ? <View>
                            <Text style={{ textAlign: 'center', marginTop: 20, color: Colors.grey }}>Searching for nearby Drivers</Text>
                            <DotIndicator size={10} color={Colors.primary} style={{ paddingBottom: 10 }} />
                        </View>
                        : <Animatable.View animation='bounceInUp' easing='ease-out' iterationCount={1} style={{ flex: 1, flexDirection: 'row' }}>

                            {this.props.driver
                                ? <Thumbnail source={{ uri: this.props.driver.photo }} />
                                : null
                            }

                            {this.props.driver
                                ? <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>

                                    <Text style={[styles.item, { fontSize: this.props.driver.name.length > 18 ? 20 : 25 }]}>{this.props.driver.name}</Text>
                                    <Text style={styles.recipient}>{this.props.driver.vehicle.plate_number} | {this.props.driver.vehicle.vehicle_type.name}</Text>

                                    {this.props.started
                                        ? <Animatable.View animation='flash' easing='ease-out' iterationCount='infinite'>
                                            <Text style={[styles.cancel, { color: Colors.green }]}><Icon name='navigate' style={{ color: Colors.green, fontSize: 12 }} />&nbsp; ON-GOING DELIVERY</Text>
                                        </Animatable.View>
                                        : <TouchableOpacity>
                                            <Text style={[styles.cancel, { color: Colors.red }]}><Icon name='close' style={{ color: Colors.red, fontSize: 12 }} />&nbsp;CANCEL REQUEST</Text>
                                        </TouchableOpacity>}

                                </View>
                                : null}

                        </Animatable.View>}

                    {this.props.driver
                        ? <Button style={{ backgroundColor: 'transparent', padding: 5 }} onPress={() => Linking.openURL(`tel:${this.props.driver.phone}`)}>
                            <Icon name='ios-call' style={{ color: Colors.primary, fontSize: 60 }} />
                        </Button>
                        : null}

                    <ConfirmDialog title='Confirm Arrival' message='Confirm that your Driver has arrived and has successfully Picked up your Parcel' visible={this.props.pickup_status} onTouchOutside={() => this.updateState({ pickup_status: false })} positiveButton={{ title: 'Confirm', onPress: () => this.updateState({ pickup_status: true }) }} negativeButton={{ title: 'Close', onPress: () => this.updateState({ pickup_status: false }) }} />

                </Footer>
            </View>
        );
    }
}

const styles = Styles.UserTrip;


const mapStateToProps = state => ({ ...state.userTrip, ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(UserTrip);
