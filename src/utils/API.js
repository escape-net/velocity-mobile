import { Platform } from 'react-native';

let BASE_URL = 'http://velocity.ng/admin/api';
//if (__DEV__) BASE_URL = 'https://93adee21.ngrok.io/api';

const Local = require('./Local');

module.exports = {

    get: (endpoint = '', token = '') => {

        return new Promise(async (resolve, reject) => {

            if (token.length == 0) {

                let user = await Local.get('user');
                token = !user ? '' : user.api_token;
            }

            let payload = {

                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            };

            fetch(`${BASE_URL}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });

        });

    },

    post: (endpoint = '', data = {}, token = '') => {

        return new Promise(async (resolve, reject) => {

            if (token.length == 0) {

                let user = await Local.get('user');
                token = !user ? '' : user.api_token;

            }

            data.device = Platform.OS;

            let payload = {

                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(data)
            };

            fetch(`${BASE_URL}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });

        });

    }
};
