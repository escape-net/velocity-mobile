module.exports = {

    primary: '#52009f',
    primaryLight: '#6701c6',
    white: '#ffffff',
    grey: '#A1ABB2',
    greyLight: '#ebebeb',
    black: '#22292F',
    green: '#009788',
    red : '#E3342F',
    backgroundGrey: '#f3f4f6'
}