import { AsyncStorage, ToastAndroid } from 'react-native';
const googleMapsKey = 'AIzaSyAMkjsL1HZcGPVN3GtV_q-RF3jH3Hm-xMQ';

module.exports = {

    save: (key, data) => {

        return new Promise(async (resolve, reject) => {

            try {

                await AsyncStorage.setItem(key, JSON.stringify(data));
                resolve(data);

            } catch (ex) {

                reject(new Error(ex.message));
            }

        })
    },

    get: (key = '') => {

        return new Promise(async (resolve, reject) => {

            try {

                let response = await AsyncStorage.getItem(key);
                resolve(JSON.parse(response));

            } catch (ex) {

                resolve(await AsyncStorage.getItem(key));
            }

        })
    },

    remove: (key = '') => {

        return new Promise(async (resolve, reject) => {

            try {

                await AsyncStorage.removeItem(key);

                resolve(true);

            } catch (ex) {

                reject(ex.message);
            }

        })

    },

    logout: () => {

        return new Promise(async (resolve, reject) => {

            try {

                let keys = ['user', 'api_token', 'role', 'persist:root'];

                await AsyncStorage.multiRemove(keys);
                await AsyncStorage.clear();

                resolve(true);

            } catch (ex) {

                reject(ex.message);
            }
        })
    },

    toast: (text = 'Could not connect at this time.') => {

        ToastAndroid.showWithGravity(text, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    },

    mapCustomStyle: () => {

        return [{ "featureType": "landscape", "elementType": "all", "stylers": [{ "hue": "#6600ff" }, { "saturation": -11 }, { "visibility": "on" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "saturation": "33" }, { "hue": "#6600ff" }, { "lightness": "2" }, { "visibility": "simplified" }] }, { "featureType": "poi.attraction", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.business", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.government", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.medical", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.park", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.place_of_worship", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.school", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "poi.sports_complex", "elementType": "all", "stylers": [{ "saturation": "-58" }, { "lightness": "28" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "hue": "#5e00ff" }, { "saturation": -79 }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.local", "elementType": "all", "stylers": [{ "lightness": 30 }, { "weight": 1.3 }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "hue": "#5e00ff" }, { "saturation": -16 }] }, { "featureType": "transit.line", "elementType": "all", "stylers": [{ "saturation": -72 }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "saturation": -65 }, { "hue": "#1900ff" }, { "lightness": 8 }] }];
    },

    googleMapsKey: () => {

        return googleMapsKey;
    },
    
	//added lat,long and accuracy to calc delta
    googleMapsDelta: ( width = 0, height = 0) => {

        const ASPECT_RATIO = width / height;
		const latDelta = 0.015; 
		const longDelta = latDelta  * ASPECT_RATIO;
        
        return { latitudeDelta: latDelta, longitudeDelta: longDelta  };
        //return { latitudeDelta, longitudeDelta };
    },

    getGooglePlace: (latitude = 0, longitude = 0, width = 0, height = 0) => {

        return new Promise((resolve, reject) => {

            let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${googleMapsKey}`;

            fetch(url).then(resp => resp.json()).then(response => {

                if (response.results != undefined && response.results.length > 0) {

                    console.log(response);

                    let place = response.results[0];
                    let viewport = place.geometry.viewport;
                    let southwest = { latitude: viewport.southwest.lat, longitude: viewport.southwest.lng };
                    let northeast = { latitude: viewport.northeast.lat, longitude: viewport.northeast.lng };

                    const ASPECT_RATIO = width / height;

                    const latitudeDelta = parseFloat(northeast.latitude) - parseFloat(southwest.latitude);
                    const longitudeDelta = latitudeDelta * ASPECT_RATIO;
				     

                    resolve({

                        address: place.formatted_address,
                        place_id: place.place_id,
                        latitude: place.geometry.location.lat,
                        longitude: place.geometry.location.lng,
                        latitudeDelta: latitudeDelta,
                        longitudeDelta: longitudeDelta
                    });

                } else {

                    resolve({ address: null, place_id: null, latitude: null, longitude: null });
                }

            }).catch(error => {

                resolve({ address: null, place_id: null, latitude: null, longitude: null, error: error.message });

            })
        })
    },

    getCurrentPosition: () => {

        return new Promise((resolve, reject) => {

            navigator.geolocation.getCurrentPosition(async position => {
				
				//also get accuracy
                let { latitude, longitude, accuracy } = position.coords;

                resolve({ latitude: latitude, longitude: longitude, accuracy : accuracy, error: null });

            }, error => {

                resolve({ latitude: 6.5244, longitude: 3.3792, error: error.message.toString() });
            },
                {
                    enableHighAccuracy: true,
                    timeout: 5000
                });
        });
    }
}