import PushNotification from 'react-native-push-notification';

export default class Notification {

    constructor(onRegister, onNotification) {

        this.configure(onRegister, onNotification);
        this.lastId = 0;
    }

    configure(onRegister, onNotification, gcm = '') {

        console.log('Configure in Notification class called');

        PushNotification.configure({

            onRegister: onRegister,
            onNotification: onNotification,
            permissions: { alert: true }
        })
    }


    localNotification() {

        console.log('localNotification Called');

        this.lastId++;

        PushNotification.localNotification({

            id: '' + this.lastId,
            title: 'Silence',
            message: 'Silence is Golden',
            actions: '["Yes", "No"]'
        })
    }

    checkPermission(callback) {

        console.log('checkPmersisn called');
        return PushNotification.checkPermissions(callback);
    }

    cancelNotification() {

        PushNotification.cancelLocalNotifications({ id: '' + this.lastId });
    }

    cancelAll() {

        PushNotification.cancelAllLocalNotifications();
    }
}