import { StyleSheet, Dimensions } from 'react-native';
import Colors from './Colors';

module.exports = {

    UserHome: StyleSheet.create({

        container: {
            height: Dimensions.get('window').height,
            width: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject,
            zIndex: -1
        },
        head: {
            paddingTop: 20,
            paddingBottom: 20,
            justifyContent: 'space-between',
            flexDirection: 'row',
        },
        pickup_box: {
            flexDirection: 'row',
            padding: 18,
            backgroundColor: Colors.white
        },
        delivery_box: {
            flexDirection: 'row',
            padding: 18,
            backgroundColor: Colors.white,
            marginTop: 0,
            borderTopColor: '#ebebeb',
            borderTopWidth: 1
        },
        footer: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: '48%',  //Changed from 60% to ensure visibility on some devices
            height: '57%',
            backgroundColor: Colors.white
        },
        footerInner: {

            padding: 0,
            width: '100%',
            padding: 10
        },
        vehicle: {
            justifyContent: 'space-around',
            margin: 10,
            alignItems: 'center',
            width: 60,
            height: 60,
            borderColor: Colors.greyLight,
            borderWidth: 1,
            borderRadius: 50
        },
        vehicleIcon: {
            padding: 5
        },
        paymentCard: {
            width: '100%',
            height: 30,
            margin: 5,
            alignSelf: 'center',
            padding: 10,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: Colors.primary
        },
        callout: {
            backgroundColor: Colors.white,
            borderWidth: 0,
            borderRadius: 2,
            width: 180,
            height: 30,
            padding: 3
        }
    }),

    UserDeliveries: StyleSheet.create({

        container: {

            backgroundColor: Colors.white,
            width: '100%',
            height: '100%'
        },
        background: {

            width: '100%',
            height: '100%'
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: '#ebebeb',
            width: '100%'
        },
        info: {
            color: Colors.black,
            textAlign: 'center',
            marginTop: '50%',
            padding: 20
        }
    }),

    UserDeliveryDetails: StyleSheet.create({

        container: {
            backgroundColor: Colors.white

        }
    }),

    UserPayments: StyleSheet.create({

        hr: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.greyLight,
            width: '100%'
        }
    }),

    UserProfile: StyleSheet.create({

        container: {

            backgroundColor: Colors.backgroundGrey,
            width: '100%',
            height: '100%'
        },
        header: {
            height: 120,
            backgroundColor: Colors.primary
        },
        top: {
            padding: 30
        },
        h1: {
            fontSize: 21,
            color: Colors.primary,
            marginBottom: 3
        },
        p: {
            fontSize: 18,
            color: Colors.black
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.greyLight,
            width: '100%'
        },
        thumbnail: {
            alignSelf: 'center'
        }
    }),

    UserRateDriver: StyleSheet.create({

        container: {
            backgroundColor: Colors.white

        },
        check: {
            color: Colors.primary,
            fontSize: 90,
            textAlign: 'center',
            paddingTop: 40,
            paddingLeft: 40,
            paddingRight: 40,
            paddingBottom: 5
        }
    }),

    UserTrip: StyleSheet.create({

        container: {
            height: Dimensions.get('window').height - 190,
            width: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject
        },
        footer: {
            width: '100%',
            backgroundColor: Colors.white,
            height: 110,
            padding: 10,
            flexDirection: 'row'
        },
        footerArrived: {
            width: '100%',
            height: 80,
            backgroundColor: Colors.white,
            height: 40
        },
        footerArrivedView: {
            padding: 10,
            marginBottom: 10,
            width: '100%'
        },
        item: {
            paddingLeft: 10,
            fontWeight: 'bold',
            color: Colors.black,
            flexWrap: 'wrap',
            width: 320
        },
        recipient: {
            paddingLeft: 10,
            color: '#aaa',
            fontSize: 15
        },
        cancel: {
            paddingLeft: 10,
            color: Colors.red,
            fontSize: 12,
            marginTop: 5
        },
    }),

    Support: StyleSheet.create({

        container: {

            backgroundColor: Colors.white,
            width: '100%',
            height: '100%'
        }
    }),

    DriverAcceptDelivery: StyleSheet.create({

        container: {
            height: Dimensions.get('window').height,
            width: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject
        },
        sender: {

            position: 'absolute',
            top: '20%',
            left: 0,
            right: 0,
            height: 250,
            backgroundColor: Colors.primary,
            opacity: 0.9,
            margin: 30,
            padding: 10,
            borderRadius: 5
        }
    }),

    DriverDeliveries: StyleSheet.create({

        container: {

            backgroundColor: Colors.white,
            width: '100%',
            height: '100%'
        },
        background: {

            width: '100%',
            height: '100%'
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: '#ebebeb',
            width: '100%'
        },
        info: {
            color: Colors.black,
            textAlign: 'center',
            marginTop: '50%',
            padding: 20
        }
    }),

    DriverHome: StyleSheet.create({

        container: {
            height: Dimensions.get('window').height - 160,
            width: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject
        },
        footer: {
            width: '100%',
            backgroundColor: Colors.white,
            height: 80,
            padding: 10
        }
    }),

    DriverProfile: StyleSheet.create({

        container: {

            backgroundColor: Colors.backgroundGrey,
            width: '100%',
            height: '100%'
        },
        header: {
            height: 120,
            backgroundColor: Colors.primary
        },
        top: {
            padding: 30
        },
        h1: {
            fontSize: 18,
            color: Colors.primary,
            marginBottom: 3
        },
        p: {
            fontSize: 14,
            color: Colors.black,
            marginBottom: 20
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.greyLight,
            width: '100%'
        },
        thumbnail: {
            alignSelf: 'center'
        }
    }),

    DriverTrip: StyleSheet.create({

        container: {
            height: Dimensions.get('window').height - 160,
            width: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
        map: {
            ...StyleSheet.absoluteFillObject
        },
        footer: {
            width: '100%',
            backgroundColor: Colors.white,
            height: 80,
            padding: 10,
            flexDirection: 'row'
        },
        footerArrived: {
            width: '100%',
            height: 80,
            backgroundColor: Colors.white,
            height: 40
        },
        footerArrivedView: {
            padding: 10,
            marginBottom: 10,
            width: '100%'
        },
        item: {
            paddingLeft: 10,
            fontWeight: 'bold',
            color: Colors.black,
            fontSize: 20,
            flexWrap: 'wrap',
            width: 320
        },
        recipient: {
            paddingLeft: 10,
            color: '#aaa',
            fontSize: 12
        },
        pipe: {
            borderRightWidth: 1,
            height: 80,
            borderRightColor: Colors.backgroundGrey
        }
    }),

    TripDetail: StyleSheet.create({

        container: {

            backgroundColor: Colors.backgroundGrey,
            width: '100%'
        },
        top: {
            padding: 30
        },
        h1: {
            fontSize: 21,
            color: Colors.primary,
            marginBottom: 3
        },
        p: {
            fontSize: 18,
            color: Colors.black
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.greyLight,
            width: '100%'
        },
        thumbnail: {
            alignSelf: 'center'
        }
    }),

    DriverTrips: StyleSheet.create({

        container: {

            backgroundColor: Colors.white,
            width: '100%',
            height: '100%'
        },
        background: {

            width: '100%',
            height: '100%'
        },
        hr: {
            borderBottomWidth: 1,
            borderBottomColor: '#ebebeb',
            width: '100%'
        },
        info: {
            color: Colors.black,
            textAlign: 'center',
            marginTop: '50%',
            padding: 20
        }
    }),

    BeginDriverUpload: StyleSheet.create({

        container: {
            backgroundColor: Colors.white
        },
        card: {
            width: '100%',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 10,
            alignSelf: 'center',
            padding: 10,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: Colors.primary,
            backgroundColor: Colors.white
        },
        h1: {
            fontSize: 17,
            fontWeight: 'bold',
            color: Colors.black
        },
        p: {
            color: Colors.black
        },
        photo: {

            alignSelf: 'flex-end'
        },
        selectText: {
            textAlign: 'right',
            fontSize: 15,
            padding: 5,
            marginBottom: 5,
            color: Colors.primary
        },
        selectTextGreen: {
            textAlign: 'right',
            fontWeight: 'bold',
            fontSize: 16,
            padding: 5,
            marginBottom: 5,
            color: Colors.green
        },
        continue: {
            paddingTop: 10,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            justifyContent: 'flex-end',
            width: '100%'
        }
    }),

    BeginDriverCategory: StyleSheet.create({

        container: {
            backgroundColor: Colors.white
        },
        card: {
            width: '100%',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 10,
            alignSelf: 'center',
            padding: 15,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: Colors.primary
        },
        h1: {
            fontSize: 20,
            fontWeight: 'bold'
        },
        continue: {
            paddingTop: 10,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            justifyContent: 'flex-end',
            width: '100%'
        }
    }),
    BeginDriverPersonalInfo: StyleSheet.create({

        container: {
            backgroundColor: Colors.white
        },
        continue: {
            paddingTop: 10,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            justifyContent: 'flex-end',
            width: '100%'
        }
    }),
    BeginDriverVehicleInfo: StyleSheet.create({

        container: {
            backgroundColor: Colors.white
        },
        continue: {
            paddingTop: 10,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            justifyContent: 'flex-end',
            width: '100%'
        }
    })
}